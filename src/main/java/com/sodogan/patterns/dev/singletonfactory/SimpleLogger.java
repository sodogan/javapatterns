/**
 * 
 */
package com.sodogan.patterns.dev.singletonfactory;

/**
 * To be used for the Logging functionality-Static classes of Loglevels
 * LogLevels can be FATAL,ERROR,WARN,TRACE,ALL etc!
 */
public final class SimpleLogger implements  ILog
{

	public static final SimpleLogger OFF ;
	public static final SimpleLogger FATAL;
	public static final SimpleLogger ERROR;
	public static final SimpleLogger WARN;
	public static final SimpleLogger INFO;
	public static final SimpleLogger DEBUG;
	public static final SimpleLogger TRACE ;
	public static final SimpleLogger ALL;

	private final String name;
	private final ILog.LogLevel logLevel;

	static {
		OFF = new SimpleLogger("OFF", ILog.LogLevel.OFF);
		FATAL = new SimpleLogger("FATAL", ILog.LogLevel.FATAL);
		ERROR = new SimpleLogger("ERROR", ILog.LogLevel.ERROR);
		WARN = new SimpleLogger("WARN", ILog.LogLevel.WARN);
		INFO = new SimpleLogger("INFO", ILog.LogLevel.INFO);
		DEBUG = new SimpleLogger("DEBUG", ILog.LogLevel.DEBUG);
		TRACE = new SimpleLogger("TRACE", ILog.LogLevel.TRACE);
		ALL = new SimpleLogger("ALL", ILog.LogLevel.ALL);
	}

	private SimpleLogger(String name, ILog.LogLevel logLevel) {
		this.name = name;
		this.logLevel = logLevel;
	}


	public String getName() {
		return name;
	}

	public ILog.LogLevel getLogLevel() {
		return logLevel;
	}


   @Override
   public void add(){
	   System.out.println("Inside the simple logg add");
   }

}
