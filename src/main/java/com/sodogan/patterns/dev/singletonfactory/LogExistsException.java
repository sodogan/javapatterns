package com.sodogan.patterns.dev.singletonfactory;

public class LogExistsException extends Throwable
{
    public LogExistsException(String message)
    {
        super(message);
    }
}
