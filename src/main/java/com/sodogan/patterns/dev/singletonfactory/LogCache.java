package com.sodogan.patterns.dev.singletonfactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/*
 * Example of Singetonfactory-
 * An Applicaion Log can be associated with unique class and Log Level
 * Each class can have only and only one Log Level
 * This is controlled through a Map where class name and Log Level is compared!
 */
public class LogCache
{

	private static final Map<Class<?>, ILog> _loglist  = new ConcurrentHashMap<>();


// No instantiation
	private LogCache(ILog log)
	{

	}

	public static ILog addLog(final Class<?> clazz, ILog log) throws LogExistsException
	{
		// check if its in the Map
		if (checkIfExistsInList(clazz)) {
         throw  new LogExistsException("Log already created for this class");
		}
		else
		{
			//add the new log to the Map
			_loglist.putIfAbsent(clazz, log);

		}

//		get the existing Log
		return getFromTheList(clazz);

	}

	private static ILog getFromTheList(final Class<?> clazz)
	{

		return _loglist.get(clazz);
	}

	private static boolean checkIfExistsInList(final Class<?> clazz)
	{
		return _loglist.containsKey(clazz) ? true : false;
	}

	/**
	 * @return the _list
	 */
	public static Map<Class<?>, ILog> getLogList()
	{
		return _loglist;
	}


}
