package com.sodogan.patterns.dev.singletonfactory;

public interface ILog
{
    enum LogLevel
    {

        /**
         * No events will be logged.
         */
        OFF,

        /**
         * A severe error that will prevent the application from continuing.
         */
        FATAL,

        /**
         * An error in the application, possibly recoverable.
         */
        ERROR,

        /**
         * An event that might possible lead to an error.
         */
        WARN,

        /**
         * An event for informational purposes.
         */
        INFO,

        /**
         * A general debugging event.
         */
        DEBUG,

        /**
         * A fine-grained debug message, typically capturing the flow through the
         * application.
         */
        TRACE,

        ALL
    }

    public default void add()
    {
        System.out.printf("default implementation");
    }

}
