/**
 * 
 */
package com.sodogan.patterns.dev.singleton;

/**
 * The Singleton pattern ensures that a class has only one instance and provides a global point of access to that instance. 
 * It is named after the singleton set, which is defined to be a set containing one element.
 * Rules of thumb:
* Abstract Factory, Builder, and Prototype can use Singleton in their implementation.
* Facade objects are often Singletons because only one Facade object is required.
* State objects are often Singletons.
 *
 */
public class SingletonEager {

	private static SingletonEager _singleton = null;
	private static final boolean _isInitialized;
	
	// Should not be created
	private SingletonEager() {
	}

	// Use to initialize settings!
	private static void init() {

	}

//Eager initialization--Only called once! 
//	same as private static SingletonEager _singleton =  new SingletonEager();
	 static  {
			try {
			_singleton = new SingletonEager();
			// call init to initialize
			init();
			_isInitialized= true;
		} catch (Exception e) {
			throw new RuntimeException("Exception occured in creating singleton instance");
		}
	}

	public static SingletonEager getInstance() {
		return _singleton;
	}

	/**
	 * @return the _isInitialized
	 */
	public static boolean isInitialized() {
		return _isInitialized;
	}

}
