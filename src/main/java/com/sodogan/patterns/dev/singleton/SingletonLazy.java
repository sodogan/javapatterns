/**
 * 
 */
package com.sodogan.patterns.dev.singleton;

/**
 * The Singleton pattern ensures that a class has only one instance and provides a global point of access to that instance. 
 * It is named after the singleton set, which is defined to be a set containing one element.
 * The Singleton design pattern is one of the most inappropriately used patterns. Singletons are intended to be used when
*   a class must have exactly one instance, no more, no less. Designers 
 * frequently use Singletons in a misguided attempt to replace global variables. A Singleton is, for intents and purposes, a global variable.
* Rules of thumb:
* Abstract Factory, Builder, and Prototype can use Singleton in their implementation.
* Facade objects are often Singletons because only one Facade object is required.
* State objects are often Singletons.
 *
 *
 */
public class SingletonLazy
{

	private static SingletonLazy _singleton = null;
	private static boolean _isInitialized;

	// private-Should not be created like this
	private SingletonLazy()
	{

	}

	// For initializing the necessary arguments!
	private static void _init()
	{

	}

//only created when the getinstance is called 
	public static SingletonLazy getInstance()
	{
		if (_singleton == null)
		{
			_singleton = new SingletonLazy();
//			call the init
			_init();
			_isInitialized = true;
		}
		return _singleton;
	}

	public static boolean IsInitialized()
	{
		return _isInitialized;
	}

}
