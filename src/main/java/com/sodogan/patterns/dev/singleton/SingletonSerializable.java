/**
 * 
 */
package com.sodogan.patterns.dev.singleton;

import java.io.Serializable;

/**
 * @author solen
 *
 */
public class SingletonSerializable implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3465157642059995614L;

	private static final SingletonSerializable _instance = new SingletonSerializable();

	//should never be created! 
	private SingletonSerializable() {
		
	}
	//get the instance here
	public static SingletonSerializable getInstance()
	{
		return _instance;
	}
	
	protected Object readResolve() {
	    return getInstance();
	}

}
