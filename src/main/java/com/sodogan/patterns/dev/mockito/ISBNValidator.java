package com.sodogan.patterns.dev.mockito;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ISBNValidator
{
    private ISBNWebService isbnWebService;
    private List<String>   validISBNList = new CopyOnWriteArrayList<>();

    public ISBNValidator(ISBNWebService isbnWebService)
    {
        this.isbnWebService = isbnWebService;
    }

    public boolean validateISBN(String ISBN)
    {
//        First check the local list if not do a webService call
        if (checkAlreadyExists(ISBN)) return true;
        if (isbnWebService.searchISBN(ISBN)) {
            validISBNList.add(ISBN);
            return true;
        }
        return false;
    }

    boolean checkAlreadyExists(String ISBN)
    {
        if (validISBNList.contains(ISBN))
            return true;
        return false;
    }


}
