package com.sodogan.patterns.dev.mockito;

public interface ISBNWebService
{
    boolean searchISBN(String isbn);
}
