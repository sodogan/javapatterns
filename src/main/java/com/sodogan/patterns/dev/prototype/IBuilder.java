package com.sodogan.patterns.dev.prototype;

public abstract class IBuilder
{

    public abstract IColor buildColor(IColor.Colors color);
    public abstract IShape buildShape(ShapeType shapeType,IColor color);

    public IShape buildColorfulShape(ShapeType shapeType, IColor.Colors colorType)
    {
        //first build the Color
        IColor color =  buildColor(colorType);
        //then build the Shape
        IShape shape =  buildShape(shapeType,color);
        return  shape;
    }
}
