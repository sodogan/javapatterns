package com.sodogan.patterns.dev.prototype;

public class RedColor implements  IColor
{

   @Override
    public String fillColor()
    {
        return ("Filled with Red Color");
    }

    @Override
   	public IColor clone()
   	{
   		// TODO Auto-generated method stub
   		return new RedColor();
   	}


}


