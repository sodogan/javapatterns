package com.sodogan.patterns.dev.prototype;

public class GreenColor implements  IColor
{

    @Override
    public String fillColor()
    {
        return ("Filled with Green Color");
    }
    
    @Override
   	public IColor clone()
   	{
   		return new GreenColor();
   	}
    
}
