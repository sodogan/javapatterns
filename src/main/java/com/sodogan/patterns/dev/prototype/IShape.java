package com.sodogan.patterns.dev.prototype;

public abstract class IShape 
{

    protected  IColor _color;

    public  IShape(IColor color){
        this._color = color;
    }
    public abstract void draw();
   
    
}
