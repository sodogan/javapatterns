package com.sodogan.patterns.dev.prototype;

public enum ShapeType
{
    CIRCLE,
    SQUARE,
    RECTANGLE
}
