package com.sodogan.patterns.dev.prototype;

public class Square extends IShape
{
    public Square(IColor color)
    {
        super(color);
    }

    @Override
    public void draw()
    {
        System.out.println("Drawing a SQUARE object with: "+ _color.fillColor());
    }
    
   
    
}
