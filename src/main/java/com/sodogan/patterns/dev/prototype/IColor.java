package com.sodogan.patterns.dev.prototype;

public interface IColor
{
    public enum Colors
    {
        RED("Red"),
        GREEN("Green"),
        BLUE("Blue");

        Colors(String name){
            colorName = name;
        }

        private String colorName;

        public String getColorName()
        {
            return colorName;
        }

    }

    String  fillColor();
//    prototype is based on the clone concept
    IColor clone();
}
