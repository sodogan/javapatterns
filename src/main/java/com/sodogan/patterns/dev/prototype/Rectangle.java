package com.sodogan.patterns.dev.prototype;

public class Rectangle extends IShape
{
    public Rectangle(IColor color)
    {
        super(color);
    }

    @Override
    public void draw()
    {
        System.out.println("Drawing a RECTANGLE object:"+ _color.fillColor());
    }

	
}
