package com.sodogan.patterns.dev.prototype;

public class Circle extends IShape
{

    public Circle(IColor color)
    {
        super(color);
    }

    @Override
    public void draw()
    {
        System.out.println("Drawing a CIRCLE object with color: "+ _color.fillColor());
    }
    
    
}
