package com.sodogan.patterns.dev.prototype;

public class BlueColor implements  IColor
{
    @Override
    public String fillColor()
    {
        return ("Filled with Blue Color");
    }
    
    @Override
   	public IColor clone()
   	{
   		return new BlueColor();
   	}
}
