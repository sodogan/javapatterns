package com.sodogan.patterns.dev.prototype;

public class ShapeBuilder extends IBuilder
{

    @Override
    public IColor buildColor(IColor.Colors color)
    {
        return ColorRegistry.getColor(color);
    }

    @Override
    public IShape buildShape(ShapeType shapeType, IColor color)
    {

        return  ShapeFactory.getShape(shapeType,color);

    }


}
