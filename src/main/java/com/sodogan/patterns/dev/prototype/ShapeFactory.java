package com.sodogan.patterns.dev.prototype;

import java.util.HashMap;
import java.util.Map;

/* Factory-method pattern is applied in here
 * You can also apply prototype and return the cloned  object
 * These two patterns often compete each other!
 */
public class ShapeFactory
{


	
    //Factory-method is applied in here
    //One way to create the Shape object
    //Other possible way is through prototype
    public static IShape getShape(ShapeType type,IColor color)
    {
        IShape any=null;
        switch ( type )
        {
            case CIRCLE:
                any = new Circle(color);
                break;
            case SQUARE:
                any = new Square(color);
                break;
            case RECTANGLE:
                any = new Rectangle(color);
                break;
        }
        return any;
    }


}
