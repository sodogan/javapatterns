package com.sodogan.patterns.dev.prototype;

import java.util.HashMap;
import java.util.Map;

/*
 * Add a clone() method to the existing "product" hierarchy.
 * Design a "registry" that maintains a cache of prototypical objects.
 * The registry could be encapsulated in a new Factory class, or
 *in the base class of the "product" hierarchy.
 *Design a factory method that: may (or may not) accept arguments,
 *finds the correct prototype object, calls clone() on that object, and returns the result.
 *The client replaces all references to the new operator with calls to the factory method.
 *
 */
public class ColorRegistry
{

    private static Map<IColor.Colors, IColor> _registry = new HashMap<>();

    //	No instantiation
    private ColorRegistry()
    {
    }

    static {
        ColorCache.buildCache(_registry);

    }

    //Get from the list and clone it
    public static IColor getColor(IColor.Colors type)
    {
        return (IColor) _registry.get(type).clone();
    }


    static class ColorCache
    {
        static void buildCache(Map<IColor.Colors, IColor> _cache)
        {
            _cache.put(IColor.Colors.RED, new RedColor());
            _cache.put(IColor.Colors.GREEN, new GreenColor());
            _cache.put(IColor.Colors.BLUE, new BlueColor());
        }
    }



}
