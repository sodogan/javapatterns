package com.sodogan.patterns.dev.decorator;

public class Anchovies extends Decorator
{
    public Anchovies(AbstractPizza abstractPizza)
    {
        super(abstractPizza);
    }

    @Override
    public double getCost()
    {
        return this.pizza.getCost() + PriceList.ANCHOVIES.getPrice();
    }
}
