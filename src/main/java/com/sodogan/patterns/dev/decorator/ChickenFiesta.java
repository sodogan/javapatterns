package com.sodogan.patterns.dev.decorator;

public class ChickenFiesta extends AbstractPizza
{
    public ChickenFiesta()
    {
        super("Chicken Fiesta");
    }

    @Override
    public double getCost()
    {
        return PriceList.CHICKENFIESTA.getPrice();
    }
}
