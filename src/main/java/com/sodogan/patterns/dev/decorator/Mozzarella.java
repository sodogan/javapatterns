package com.sodogan.patterns.dev.decorator;

public class Mozzarella extends  Decorator
{
    public Mozzarella(AbstractPizza pizza)
    {
        super(pizza);
    }

    @Override
    public double getCost()
    {
        return this.pizza.getCost() + PriceList.MOZZARELLA.getPrice();
    }
}
