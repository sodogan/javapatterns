package com.sodogan.patterns.dev.decorator;
/*
* Extending a class is the first thing that comes to mind when you need to alter an object’s behavior.
* However, inheritance has several serious caveats that you need to be aware of.
* Inheritance is static. You can’t alter the behavior of an existing object at runtime.
* Supertype of all classes:Pizza
 */
public abstract class AbstractPizza
{
   protected  String description;
   protected   double cost;

   public AbstractPizza(String description)
   {
      this.description = description;
   }

   public AbstractPizza()
   {
   }

   public String getDescription()
   {
      return description;
   }
// THE COMMON BEHAVIOUR TO BOTH Decorator and Decoratee
   public abstract double getCost();

}
