package com.sodogan.patterns.dev.decorator;

public class Margarita extends AbstractPizza
{
    public Margarita()
    {
        super("Margarita");
    }

    @Override
    public double getCost()
    {
        return PriceList.MARGARITA.getPrice();
    }
}
