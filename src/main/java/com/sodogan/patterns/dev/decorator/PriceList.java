package com.sodogan.patterns.dev.decorator;

public enum PriceList
{
    MARGARITA(6.2),
    MEATLOVERS(7.2),
    CHICKENFIESTA(7.5),
    VEGETERIAN(7.8),
    MUSHROOMS(0.4),
    MOZZARELLA(0.5),
    JALAPENO(0.6),
    PEPPERONI(0.7),
    ANCHOVIES(1.1),
    ;

    private double price;

    PriceList(double price)
    {
        this.price = price;
    }

    public double getPrice()
    {
        return price;
    }


}
