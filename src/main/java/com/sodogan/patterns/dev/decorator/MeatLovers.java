package com.sodogan.patterns.dev.decorator;

public class MeatLovers extends AbstractPizza
{
    public MeatLovers()
    {
        super("Meat Lovers");
    }

    @Override
    public double getCost()
    {
        return PriceList.MEATLOVERS.getPrice();
    }
}
