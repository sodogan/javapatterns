package com.sodogan.patterns.dev.decorator;

public class Vegeterian extends  AbstractPizza
{
    public Vegeterian()
    {
        super("Vegaterian");
    }

    @Override
    public double getCost()
    {
        return PriceList.VEGETERIAN.getPrice();    }
}
