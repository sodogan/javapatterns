package com.sodogan.patterns.dev.decorator;

public class Pepperoni extends  Decorator
{
    public Pepperoni(AbstractPizza abstractPizza)
    {
        super(abstractPizza);
    }

    @Override
    public double getCost()
    {
        return this.pizza.getCost() + PriceList.PEPPERONI.getPrice();
    }
}
