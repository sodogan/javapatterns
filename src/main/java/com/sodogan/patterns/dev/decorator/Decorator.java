package com.sodogan.patterns.dev.decorator;
/*
* Attach additional responsibilities to an object dynamically.
* Decorators provide a flexible alternative to subclassing for extending functionality.
* Client-specified embellishment of a core object by recursively wrapping it.
* Wrapping a gift, putting it in a box, and wrapping the box.
* See that Decorator has a Pizza and implements the Pizza
* Implementing the same interface or extending the same class gives Decorator flexibility like other Pizzas
*/
public abstract  class Decorator extends AbstractPizza
{

    protected AbstractPizza pizza;

    public Decorator(AbstractPizza abstractPizza)
    {
        super();
        this.pizza = abstractPizza;
    }


}
