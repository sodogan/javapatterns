package com.sodogan.patterns.dev.decorator;

public class Jalepeno extends Decorator
{
    public Jalepeno(AbstractPizza pizza)
    {
        super(pizza);
    }

    @Override
    public double getCost()
    {
        return this.pizza.getCost() + PriceList.JALAPENO.getPrice();
    }
}
