package com.sodogan.patterns.dev.decorator;

public class Mushrooms extends Decorator
{
    public Mushrooms(AbstractPizza abstractPizza)
    {
        super(abstractPizza);
    }

    @Override
    public double getCost()
    {
        return this.pizza.getCost() + PriceList.MUSHROOMS.getPrice();
    }
}
