package com.sodogan.patterns.dev.composite;


import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class SimpleFile extends File implements IDisplayable
{


    public SimpleFile(String dirPath)
    {
        super(dirPath);

    }


    public void ls()
    {
        System.out.println("  File:"+ super.toString());
    }



    @Override
    public String toString()
    {
        return "SimpleFile{" +
                "file=" + super.toString() +
                '}';
    }
}

