package com.sodogan.patterns.dev.composite;


/*
 * Compose objects into tree structures to represent whole-part hierarchies.
 * Composite lets clients treat individual objects and compositions of objects uniformly.
 * Recursive composition needs to exist!
 * "Directories contain entries, each of which could be a directory."
 * 1-to-many "has a" up the "is a" hierarchy
 * Composite doesn't force you to treat all Components as Composites.
 * It merely tells you to put all operations that you want to treat "uniformly" in the Component class.
 * If add, remove, and similar operations cannot, or must not, be treated uniformly, then do not put them in the Component base class.
 * it merely depicts what in our experience is a common realization thereof.
 * Just because Composite's structure diagram shows child management operations in the Component base class
 * doesn't mean all implementations of the pattern must do the same.
 * Transparency over Safety
 * */
@FunctionalInterface
public interface IDisplayable
{
    void ls();

// Not needed really in here-just to prove that can have a static method in an Functional interface
    static boolean isDir(IDisplayable displayable)
    {
//        if (displayable instanceof Directory) return  true;
//        else if (displayable instanceof Directory) return  false;
      return  (displayable instanceof Directory) ? true: false ;
    }
}
