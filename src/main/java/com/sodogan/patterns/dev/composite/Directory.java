package com.sodogan.patterns.dev.composite;

import java.util.ArrayList;
import java.util.List;

public class Directory implements IDisplayable
{
    private final String dirPath;

    private List<IDisplayable> list = new ArrayList<>();



    public Directory(String dirPath)
    {
        this.dirPath = dirPath;
    }


    public List<IDisplayable> getList()
    {
        return list;
    }

    @Override
    public void ls()
    {
        System.out.println(toString());
        for (IDisplayable displayable : list) {
            displayable.ls();
        }
    }



    public void add(IDisplayable displayable)
    {
        list.add(displayable);
    }

    public void remove(IDisplayable displayable)
    {
        list.remove(displayable);
    }


    public String getDirPath()
    {
        return dirPath;
    }

    @Override
    public String toString()
    {
        return "Directory{" +
                "dirPath='" + dirPath + '\'' +
                ", list=" + list +
                '}';
    }


}
