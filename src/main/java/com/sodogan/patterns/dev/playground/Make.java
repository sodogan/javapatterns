package com.sodogan.patterns.dev.playground;

public abstract class Make implements IMake
{

    private String manufacturer;

    public Make(String manufacturer)
    {
        this.manufacturer = manufacturer;
    }

    @Override
    public String getMakeDetails()
    {
        return this.toString();
    }


    @Override
    public String toString()
    {
        return "Make{" +
                "manufacturer='" + manufacturer + '\'' +
                '}';
    }

}
