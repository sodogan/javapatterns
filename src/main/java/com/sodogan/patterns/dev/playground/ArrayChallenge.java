package com.sodogan.patterns.dev.playground;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ArrayChallenge
{
    //    With sorting
    public static Map<Integer, Integer> howManyTimes(int[] anyArray)
    {
        Map<Integer, Integer> result = new HashMap<>();
//sort the array
        int index = 0, currentIndex = 0;
        Arrays.sort(anyArray);
        if(anyArray.length == 0){
            return  result;
        }
        else if(anyArray.length == 1){
            result.put(anyArray[0],1 );
            return  result;
        }

        currentIndex = 1;
        while (currentIndex < anyArray.length) {
//            get the value
            if(currentIndex == anyArray.length-1) System.out.println("last one is "+anyArray[currentIndex]);
            int prevValue = anyArray[index];
            int currentValue = anyArray[currentIndex];
            if(prevValue == currentValue){
                result.put(prevValue,2 );
                index ++;
                currentIndex++;
            }
            else{
                result.put(currentValue,1 );
                result.put(prevValue,1 );
            }
            index = currentIndex;

            currentIndex++;

        }

        return result;
    }
}
