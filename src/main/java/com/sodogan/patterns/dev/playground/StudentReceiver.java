package com.sodogan.patterns.dev.playground;

public class StudentReceiver
{

        private volatile Student student;

        public  StudentReceiver(){
            student = new Student();
        }

        void testMultiThread(Student student1, Student student2){

            Thread thread1 = new Thread( ()->this.student.setID(student1.getID()).setAddress(student1.getAddress()).setID(student1.getID()) );
            Thread thread2 = new Thread( ()->this.student.setID(student2.getID()).setAddress(student2.getAddress()).setID(student2.getID()) );

            thread1.start();
            thread2.start();
        }

        String getStudentInfo(){
            return  student.toString();
        }


}
