package com.sodogan.patterns.dev.playground;

public interface IEngine
{
    public String getEngineDetails();

    void start();
    void stop();
}
