package com.sodogan.patterns.dev.playground;

public abstract class Model implements  IModel
{


    protected String model;

    public  Model(String  model){
        this.model = model;
    }

    @Override
    public String getModelDetails()
    {
        return toString();
    }


    @Override
    public String toString()
    {
        return "Model{" +
                "model='" + model + '\'' +
                '}';
    }

}
