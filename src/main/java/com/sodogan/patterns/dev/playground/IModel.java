package com.sodogan.patterns.dev.playground;

public interface IModel
{
    public abstract String getModelDetails();
}
