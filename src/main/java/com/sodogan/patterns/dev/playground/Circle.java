package com.sodogan.patterns.dev.playground;

public class Circle implements  IShape
{

    private  int size;

    @Override
    public void draw()
    {
        System.out.println("Drawing a circle");
    }

    public void setSize(int size){
        this.size = size;
    }

    @Override
    public String toString()
    {
        return "Circle{" +
                "size=" + size +
                '}';
    }


}
