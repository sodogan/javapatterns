package com.sodogan.patterns.dev.playground;

public class CarBuilder
{
    //    protected  ICar car;
//
//    public CarBuilder(ICar car)
//    {
//        this.car = car;
//    }
//
//    public void setEngine()
//    {
//        this.car.s
//    }
    private String model;
    private String make;

    public CarBuilder setModel(String model)
    {
        this.model = model;
        return this;
    }

    public CarBuilder setMake(String make)
    {
        this.make = make;
        return this;
    }


    public MyCar buildCar()
    {
        return new MyCar(model, make);
    }

    public class MyCar
    {

        private String model;
        private String make;


        public MyCar(String model, String make)
        {
            this.model = model;
            this.make = make;

        }
    }


}
