package com.sodogan.patterns.dev.playground;

public interface ICar
{
    public abstract void start();
    public abstract void stop();
}
