package com.sodogan.patterns.dev.playground;

public abstract class ShapeFactory
{
    public static IShape _shape;

    public static IShape createCircle()
    {
        if (_shape == null) {
            _shape = new Circle();
        }
        return _shape;
    }


}
