package com.sodogan.patterns.dev.playground;

import java.util.EmptyStackException;

public abstract class Engine implements  IEngine
{
    protected double engine_size;

    protected Engine(double engine_size){
        this.engine_size = engine_size;
    }
    @Override
    public String getEngineDetails()
    {
        return toString();
    }

    @Override
    public String toString()
    {
        return "Engine{" +
                "engine_size=" + engine_size +
                '}';
    }
}
