package com.sodogan.patterns.dev.playground;

import com.sodogan.patterns.dev.abstractfactory.ICPU;
import com.sodogan.patterns.dev.abstractfactory.Intel_CPU;


public abstract class Car implements ICar
{

    private IEngine engine;
    private IMake   make;
    private IModel  model;
    private CarType carType;


    public Car(IEngine engine, IMake make, IModel model, CarType carType)
    {
        this.engine = engine;
        this.make = make;
        this.model = model;
        this.carType = carType;
    }

    @Override
    public void start()
    {
        engine.start();
    }

    @Override
    public void stop()
    {
        engine.stop();
    }


}
