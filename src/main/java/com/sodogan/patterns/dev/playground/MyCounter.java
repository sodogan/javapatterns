package com.sodogan.patterns.dev.playground;

public class MyCounter
{
    private int count = 0;

    public void increment() throws InterruptedException
    {
        int temp = count;
        count = temp + 1;
//        synchronized (this) {
//            int temp = count;
//            count = temp + 1;
//        }
    }

    public  int getCount()
    {
        return count;
    }
}
