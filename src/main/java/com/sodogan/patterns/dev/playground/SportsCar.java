package com.sodogan.patterns.dev.playground;

public class SportsCar extends  Car
{

    public SportsCar(IEngine engine, IMake make, IModel model, CarType carType)
    {
        super(engine, make, model, carType);
    }
}
