package com.sodogan.patterns.dev.playground;

public class Student
{

    private volatile int    ID;
    private volatile String name;
    private volatile String address;


    public Student setID(int id)
    {
        this.ID = id;
        return this;
    }

    public Student setName(String name)
    {
        this.name = name;
        return this;
    }

    public Student setAddress(String address)
    {
        this.address = address;
        return this;
    }

    public int getID()
    {
        return ID;
    }

    public String getName()
    {
        return name;
    }

    public String getAddress()
    {
        return address;
    }


    @Override
    public String toString()
    {
        return "Student{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }



}
