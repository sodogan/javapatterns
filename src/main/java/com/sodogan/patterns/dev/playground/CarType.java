package com.sodogan.patterns.dev.playground;

public enum CarType
{
    SPORTSCAR,
    WAGON,
    STATION,
    FAMILY
}
