package com.sodogan.patterns.dev.playground;

public class LoopRunner
{
    public void testBreak()
    {
        int UPPER_LIMIT = 2;

        print("Before for loop");
        for(int i = 0; i < UPPER_LIMIT; i++){
            print(String.format("inside first for loop index:%s ",i));
            for(int j = 0; j < UPPER_LIMIT; j++) {
                if(j == 1) return;
                print(String.format("inside second for loop index:%s ",j));

            }
            print("After second loop insidefirst for loop");
        }
        print("End of all for loops");
    }

//    print the current
    <T> void print( T message){
        System.out.println(message);
    }

}
