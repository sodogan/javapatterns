package com.sodogan.patterns.dev.playground;

public interface IShape
{
    public  void  draw();
}
