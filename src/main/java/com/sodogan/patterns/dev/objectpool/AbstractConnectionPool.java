package com.sodogan.patterns.dev.objectpool;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Hashtable;

public abstract class AbstractConnectionPool
{
    private static final long                        DEFAULT_EXPIRATION_TIME = 30000;
    private              long                        expirationTime;
    private              Hashtable<Connection, Long> lockedConnections, freeConnections;

    public AbstractConnectionPool()
    {
        expirationTime = DEFAULT_EXPIRATION_TIME;
        lockedConnections = new Hashtable<Connection, Long>();
        freeConnections = new Hashtable<Connection, Long>();
    }

    public AbstractConnectionPool(long expirationTime)
    {
        expirationTime = expirationTime;
        lockedConnections = new Hashtable<Connection, Long>();
        freeConnections = new Hashtable<Connection, Long>();
    }

    //To be implementted in the children
    protected abstract Connection create() throws SQLException;

    public abstract void expire(Connection connection) throws SQLException;

    public abstract boolean validate(Connection connection) throws SQLException;

    public Connection checkOut() throws SQLException
    {
        long now = System.currentTimeMillis();
        Connection result = null;
//        expire the ones that are already expired

        if (freeConnections.size() > 0) {
            Enumeration<Connection> keys = freeConnections.keys();
            while (keys.hasMoreElements()) {
                Connection connection = keys.nextElement();
                long timeStamp = freeConnections.get(connection);
                if (now - timeStamp > expirationTime) {
//                  expire
                    expire(connection);
                }
                else{
                    if(validate(connection)){
                        freeConnections.remove(connection);
                        lockedConnections.put(connection,now);
                        return  connection;
                    }
                }
            }

        }

//            create
        result = create();
        lockedConnections.put(result, now);
        return (result);


    }

    public void checkIn(Connection connection)
    {
        long now = System.currentTimeMillis();
        //        remove from the locked
        lockedConnections.remove(connection);
//         add to the unlocked
        freeConnections.put(connection, now);
    }
}
