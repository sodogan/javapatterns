package com.sodogan.patterns.dev.objectpool;

import com.sodogan.patterns.dev.singletonfactory.SimpleLogger;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class ObjectPool
{

    //    two lists
    private List<PoolResource> locked   = new ArrayList<>();
    private List<PoolResource> unLocked = new ArrayList<>();

    private ObjectPool()
    {

    }

    public PoolResource checkOut()
    {
        PoolResource resource = null;

        if (unLocked.size() == 0) {
            resource = create();
            unLocked.add(resource);
        }
        int lastIndex = unLocked.size() - 1;
        resource = unLocked.get(lastIndex);
        unLocked.remove(lastIndex);
//        checkin
        locked.add(resource);

        return resource;
    }

    @NotNull
    private PoolResource create()
    {
        return new PoolResource();
    }

    private boolean checkIn(PoolResource resource)
    {
        locked.add(resource);
        return  true;
    }


    private static class SingletonHelper
    {
        private static ObjectPool SELF = new ObjectPool();

    }

    public static final ObjectPool getInstance()
    {
        return SingletonHelper.SELF;
    }


}
