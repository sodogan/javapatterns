package com.sodogan.patterns.dev.objectpool;

import java.sql.*;
import java.util.Properties;

public class JDBCConnectionPool extends  AbstractConnectionPool
{

    public static final String     USERNAME = "root";
    public static final String     PASSWORD = "Taksim12";
    private             Properties properties;
    private             String     connectionURL;


    public JDBCConnectionPool(Properties properties, String connectionURL)
    {
        this.properties = properties;
        this.connectionURL = connectionURL;
    }

    public JDBCConnectionPool(String connectionURL)
    {
        this.properties = new Properties();
        this.connectionURL = connectionURL;
//
        properties.put("user", USERNAME);
        properties.put("password", PASSWORD);

    }

    @Override
    protected Connection create() throws SQLException
    {
        try {
            Connection connection = DriverManager.getConnection(connectionURL, properties);
            if (connection != null) {
                System.out.println("Successfully connected to MySQL database test");
                return connection;
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
            return (null);
        }
        return (null);
    }
    @Override
    public void expire(Connection connection) throws SQLException
    {
        try {
            ((Connection) connection).close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean validate(Connection connection) throws SQLException
    {
        try {
//            return (!((Connection) connection).isClosed());
            if(connection.isClosed() == true)
            {
                return  false;
            }
            else{
                return  true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return (false);
        }
    }


//    public static void testJDBCConnection() throws ClassNotFoundException
//    {
//        String sqlSelectAllCities = "SELECT * FROM city";
////        String connectionUrl = "jdbc:mysql://localhost:3306/world";
//        String connectionUrl = "jdbc:mysql://localhost:3306/world";
//// Given -When
//        try {
//            Properties info = new Properties();
//            // load and register JDBC driver for MySQL
//            Connection connection = DriverManager.getConnection(connectionUrl, info);
//            if (connection != null) {
//                System.out.println("Successfully connected to MySQL database test");
//            }
//
//            Statement stmt = connection.createStatement();
//            ResultSet rs = stmt.executeQuery(sqlSelectAllCities);
//            while (rs.next()) {
//                System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getString(3));
//            }
//            connection.close();
////     Then
////        assertTrue(connection.isValid());
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }


}
