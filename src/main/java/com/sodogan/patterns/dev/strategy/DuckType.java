package com.sodogan.patterns.dev.strategy;

public enum DuckType
{
	MALLARD, REDHEADED, DECOY, RUBBER
}
