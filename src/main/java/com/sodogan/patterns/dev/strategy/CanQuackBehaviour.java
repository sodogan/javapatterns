package com.sodogan.patterns.dev.strategy;

public class CanQuackBehaviour implements IQuackBehaviour
{

    private IQuackBehaviour.QuackType quackType;

    public CanQuackBehaviour()
    {
        this.quackType = QuackType.QUACK ;
      }
    @Override
    public QuackType performQuack()
    {
        return  this.quackType;
    }

    public QuackType getQuackType()
    {
        return quackType;
    }


}
