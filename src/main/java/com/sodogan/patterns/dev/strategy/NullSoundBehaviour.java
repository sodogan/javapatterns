package com.sodogan.patterns.dev.strategy;

public class NullSoundBehaviour implements IQuackBehaviour
{
    @Override
    public QuackType performQuack()
    {
        return  QuackType.NULL;
    }


}
