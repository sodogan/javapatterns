package com.sodogan.patterns.dev.strategy;

public class RedHeadedDuck extends Duck
{
    public RedHeadedDuck( IFlyBehaviour flyBehaviour, IQuackBehaviour quackBehaviour)
    {
        super("Aythya americana", flyBehaviour, quackBehaviour);
    }


    @Override
    public void swim()
    {

    }

    @Override
    public void display()
    {
        System.out.println(" I am a RedHeaded Duck with scientific name: "+ getScientificName());

    }
}
