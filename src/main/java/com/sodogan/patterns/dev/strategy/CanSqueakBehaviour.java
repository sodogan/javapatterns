package com.sodogan.patterns.dev.strategy;

public class CanSqueakBehaviour implements IQuackBehaviour
{
    private IQuackBehaviour.QuackType quackType;

    public CanSqueakBehaviour()
    {
        quackType =  QuackType.SQEEK ;
    }

    @Override
    public QuackType performQuack()
    {
        return this.quackType;
    }
}
