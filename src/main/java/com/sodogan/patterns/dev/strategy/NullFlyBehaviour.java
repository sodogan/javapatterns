package com.sodogan.patterns.dev.strategy;

/*
* Applying Null Object Pattern here!
* A Null Object can be a special case of the Strategy pattern. Strategy specifies several ConcreteStrategy classes
* as different approaches for accomplishing a task. If one of those approaches is to consistently do nothing,
* that ConcreteStrategy is a NullObject. F
* or example, a Controller is a View's Strategy for handling input, and NoController is the Strategy that ignores all input.
 */

public class NullFlyBehaviour implements IFlyBehaviour{

    private  int flight_height;

    public NullFlyBehaviour(){
        flight_height = Flight_Height.NONE.getHeight();
    }

    @Override
    public int performFly()
    {
        return flight_height;
    }



}
