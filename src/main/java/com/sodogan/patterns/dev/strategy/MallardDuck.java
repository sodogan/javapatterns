package com.sodogan.patterns.dev.strategy;

public class MallardDuck extends Duck
{
    public MallardDuck( IFlyBehaviour flyBehaviour, IQuackBehaviour quackBehaviour)
    {
        super("Anas platyrhynchos", flyBehaviour, quackBehaviour);
    }


    @Override
    public void swim()
    {

    }

    @Override
    public void display()
    {
        System.out.println(" I am a Mallard Duck with scientific name: "+ getScientificName());
    }
}
