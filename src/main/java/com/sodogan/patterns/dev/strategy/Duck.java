package com.sodogan.patterns.dev.strategy;
/*
* Duck is started by a inheritance relationship -All ducks should fly swim quack
* Duck: MallardDuck,RedHeadedDuck was OK but adding
* DecoyDuck Then RubberDuck broke the inheritance and design
* As rubberducks dont fly and decoyducks dont quack nor fly
* This shows how fragile inhertiance is
* Define a family of algorithms, encapsulate each one, and make them interchangeable.
* Strategy lets the algorithm vary independently from the clients that use it.
* Capture the abstraction in an interface, bury implementation details in derived classes.
* Rules of thumb
* Strategy is like Template Method except in its granularity.
* State is like Strategy except in its intent.
* Strategy lets you change the guts of an object. Decorator lets you change the skin.
* State, Strategy, Bridge (and to some degree Adapter) have similar solution structures.
* They all share elements of the 'handle/body' idiom. They differ in intent - that is, they solve different problems.
* Strategy has 2 different implementations,
* the first is similar to State. The difference is in binding times
* (Strategy is a bind-once pattern, whereas State is more dynamic).
* Strategy objects often make good Flyweights.
 */
public abstract class Duck
{

    protected  String  scientificName;
    // Some ducks do not fly or quack-Encapsulate what varies
    protected  IFlyBehaviour flyBehaviour;
    protected  IQuackBehaviour quackBehaviour;

    public Duck(String scientificName, IFlyBehaviour flyBehaviour, IQuackBehaviour quackBehaviour)
    {
        this.scientificName = scientificName;
        this.flyBehaviour = flyBehaviour;
        this.quackBehaviour = quackBehaviour;
    }

    //All ducks Quack and display
    public abstract void swim();
    public  abstract   void display();

// Notice that the  fly  is delegated to  the strategy
    public int performFly()
    {
         return flyBehaviour.performFly();

    }
// Notice that the  quack  is delegated to  the strategy
    public IQuackBehaviour.QuackType performQuack(){

        return quackBehaviour.performQuack();
    }

// Gives dynamic behaviour
    public void setFlyBehaviour(IFlyBehaviour flyBehaviour)
    {
        this.flyBehaviour = flyBehaviour;
    }

    public void setQuackBehaviour(IQuackBehaviour quackBehaviour)
    {
        this.quackBehaviour = quackBehaviour;
    }

    public String getScientificName()
    {
        return scientificName;
    }

    public IFlyBehaviour getFlyBehaviour()
    {
        return flyBehaviour;
    }

    public IQuackBehaviour getQuackBehaviour()
    {
        return quackBehaviour;
    }

}
