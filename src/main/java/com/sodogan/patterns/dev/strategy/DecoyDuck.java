package com.sodogan.patterns.dev.strategy;

/* Decoy Ducks dont fly or quack
 *  Liskov Substition principle is violated if use Inheritance!
 */
public class DecoyDuck extends Duck
{
    public DecoyDuck( IFlyBehaviour flyBehaviour, IQuackBehaviour quackBehaviour)
    {
        super("None", flyBehaviour, quackBehaviour);
    }



    @Override
    public void swim()
    {

    }

    @Override
    public void display()
    {
        System.out.println(" I am a Decoy Duck with scientific name: "+ getScientificName());

    }
}
