package com.sodogan.patterns.dev.strategy;

public class CanFlyHighBehaviour implements IFlyBehaviour
{

    private int height;

    public CanFlyHighBehaviour()
    {
        this.height = IFlyBehaviour.Flight_Height.HIGH.getHeight();
    }


    @Override
    public int performFly()
    {
        return  getHeight();
     }

    @Override
    public int getHeight()
    {
        return height;
    }


}
