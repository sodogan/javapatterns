package com.sodogan.patterns.dev.strategy;

public interface IFlyBehaviour
{
    enum Flight_Height{
        NONE(0),
        LOW(10),
        HIGH(100)
        ;
        private int height;

        Flight_Height(int height)
        {
            this.height = height;
        }

        public int getHeight()
        {
            return height;
        }
    }
    //Not all ducks fly or swim
    //Encapsulate what varies
    int performFly();
    default int getHeight(){ return  Flight_Height.NONE.getHeight() ;};

}
