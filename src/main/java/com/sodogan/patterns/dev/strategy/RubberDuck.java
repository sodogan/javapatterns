package com.sodogan.patterns.dev.strategy;

/* Rubber  Ducks dont fly or quack-they squeek
 *  Liskov Substition principle is violated if use Inheritance!
 */
public class RubberDuck extends Duck
{


    public RubberDuck(IFlyBehaviour flyBehaviour, IQuackBehaviour quackBehaviour)
    {
        super("None", flyBehaviour, quackBehaviour);
    }

    @Override
    public void swim()
    {

    }

    @Override
    public void display()
    {
        System.out.println(" I am a Rubber Duck with scientific name: "+ getScientificName());

    }
}
