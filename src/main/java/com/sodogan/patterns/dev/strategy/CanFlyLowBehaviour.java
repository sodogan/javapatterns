package com.sodogan.patterns.dev.strategy;

public class CanFlyLowBehaviour implements IFlyBehaviour
{
    private int height;

    public CanFlyLowBehaviour()
    {
        this.height = IFlyBehaviour.Flight_Height.LOW.getHeight();
    }


    @Override
    public int performFly()
    {
       return getHeight();
    }

    @Override
    public int getHeight()
    {
        return height;
    }
}
