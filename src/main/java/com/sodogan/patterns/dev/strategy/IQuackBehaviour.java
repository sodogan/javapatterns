package com.sodogan.patterns.dev.strategy;

public interface IQuackBehaviour
{
    enum QuackType{
        QUACK,
        SQEEK,
        NULL
    }
    QuackType performQuack();
}
