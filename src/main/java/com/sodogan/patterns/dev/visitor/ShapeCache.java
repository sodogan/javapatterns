package com.sodogan.patterns.dev.visitor;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class ShapeCache
{

    private static  final ConcurrentMap<IShape.Shape_Type, IShape> cache = new ConcurrentHashMap<>();


    static {
        loadCache();
    }


    private static void loadCache()
    {
        IShape composite = new Composite();
        cache.put(IShape.Shape_Type.COMPOSITE, composite);

        IShape square = new Square();
        cache.put(IShape.Shape_Type.SQUARE, square);

        IShape rectangle = new Rectangle(0,0);
        cache.put(IShape.Shape_Type.RECTANGLE, rectangle);

        IShape triangle = new Triangle();
        cache.put(IShape.Shape_Type.TRIANGLE, triangle);

    }

    public static IShape getShape(IShape.Shape_Type shapeType) throws CloneNotSupportedException
    {
        IShape shape = cache.get(shapeType);
        return (IShape) shape.clone();

    }

}
