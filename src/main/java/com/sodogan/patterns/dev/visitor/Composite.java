package com.sodogan.patterns.dev.visitor;

import java.util.ArrayList;
import java.util.List;

public class Composite implements IShape
{

    private Shape_Type type;

    private List<IShape> shapeList = new ArrayList<>();

    public Composite()
    {
        this.type = Shape_Type.COMPOSITE;
    }


    @Override
    public void draw()
    {
//        Lop each shape and call the draw!
        shapeList.forEach((shape) -> shape.draw());

    }

    public void addShape(IShape shape)
    {
        shapeList.add(shape);
    }

    @Override
    public Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }


    @Override
    public String export(IExportVisitor visitor)
    {

        return visitor.exportComposite(this);
    }

    public List<IShape> getShapeList()
    {
        return shapeList;
    }


    @Override
    public Shape_Type getShapeType()
    {
        return this.type;
    }


}
