package com.sodogan.patterns.dev.visitor;

public class Rectangle implements  IShape
{

    private  Shape_Type type;


    private  int width;
    private  int height;

    public Rectangle(int width, int height)
    {
        this.width = width;
        this.height = height;
        this.type = Shape_Type.RECTANGLE;
    }


    @Override
    public void draw()
    {
        System.out.println("Drawing type of: "+ type);

    }

    @Override
    public Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }

    @Override
    public String export(IExportVisitor visitor)
    {
        return visitor.exportRectangle(this);
    }

    @Override
    public Shape_Type getShapeType()
    {
        return this.type;
    }


    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }


}
