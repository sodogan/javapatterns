package com.sodogan.patterns.dev.visitor;

public class Triangle implements  IShape
{
    private  Shape_Type type;

    public Triangle(){
        this.type = Shape_Type.TRIANGLE;
    }

    @Override
    public void draw()
    {
        System.out.println("Drawing type of: "+ type);


    }

    @Override
    public Shape_Type getShapeType()
    {
        return this.type;
    }


    @Override
    public Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }

    @Override
    public String export(IExportVisitor visitor)
    {

        return visitor.exportTriangle(this);
    }

}
