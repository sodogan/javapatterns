package com.sodogan.patterns.dev.visitor;

public class Square implements  IShape
{

    private  Shape_Type type;

    public Square(){
        this.type = Shape_Type.SQUARE;
    }


    @Override
    public void draw()
    {
        System.out.println("Drawing type of: "+ type);

    }

    @Override
    public Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }

    @Override
    public String export(IExportVisitor visitor)
    {
        return visitor.exportSquare(this);
    }

    @Override
    public Shape_Type getShapeType()
    {
        return this.type;
    }




}
