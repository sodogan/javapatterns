package com.sodogan.patterns.dev.visitor;
/*
* In this example, theres Composite pattern(also prototype in ShapeCache)
* we would want to add export functionality into XML.
* The catch is that we don’t want to change the code of shapes directly or at least keep it to the minimum.
*
* You can treat Visitor as a powerful version of the Command pattern.
* Its objects can execute operations over various objects of different classes.
* You can use Visitor to execute an operation over an entire Composite tree.
* You can use Visitor along with Iterator to traverse a complex data structure and
* execute some operation over its elements, even if they all have different classes.
 */
public interface IShape extends  Cloneable
{
    enum Shape_Type{
        COMPOSITE,
        SQUARE,
        RECTANGLE,
        TRIANGLE
    }
    void draw();
//    Clone necessary for prototype
    Object clone() throws CloneNotSupportedException;

//  Export is not related with this so We apply Visitor pattern
    String export(IExportVisitor visitor);

    Shape_Type getShapeType();
}
