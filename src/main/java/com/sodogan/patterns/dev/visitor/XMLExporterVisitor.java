package com.sodogan.patterns.dev.visitor;
/*
* The Visitor pattern suggests that you place the new behavior into a separate class called visitor,
* instead of trying to integrate it into existing classes.
* The original object that had to perform the behavior is now passed to one of the visitor’s methods as an argument,
* providing the method access to all necessary data contained within the object.
* Use the Visitor when you need to perform an operation on all elements of a complex object structure (for example, an object tree).
*
 */
public class XMLExporterVisitor implements IExportVisitor
{
    public String exportToXML(IShape... shapes)
    {
//        Loop for each and call the export
        StringBuilder stringBuilder = new StringBuilder();
        for (IShape shape : shapes) {
            stringBuilder.append(shape.export(this));
        }
       return stringBuilder.toString();
    }

    @Override
    public String exportComposite(Composite composite)
    {
        return "<composite>" + "\n" +
                "    <dtls>" + exportChildren(composite)+ "</dtls>" + "\n" +
                "</composite>";
    }

    public String exportChildren(Composite composite)
    {
        StringBuilder sb = new StringBuilder();
        for (IShape shape : composite.getShapeList()) {
            String obj = shape.export(this);
            // Proper indentation for sub-objects.
            obj = "    " + obj.replace("\n", "\n    ") + "\n";
            sb.append(obj);
        }
        return sb.toString();
    }

    @Override
    public String exportSquare(Square square)
    {
        return "<square>" + "\n" +
                "    <dtls>" + square.toString() + "</dtls>" + "\n" +
                "</square>";
    }

    @Override
    public String exportRectangle(Rectangle rectangle)
    {
        return "<rectangle>" + "\n" +
                "    <dtls>" + rectangle.toString() + "</dtls>" + "\n" +
                "</rectangle>";
    }

    @Override
    public String exportTriangle(Triangle triangle)
    {
        return "<triangle>" + "\n" +
                "    <dtls>" + triangle.toString() + "</dtls>" + "\n" +
                "</triangle>";
    }


}
