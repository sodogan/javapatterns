package com.sodogan.patterns.dev.visitor;
/*
* Visitor is a behavioral design pattern that lets you separate algorithms
* from the objects on which they operate.
* However, the Visitor pattern addresses this problem. It uses a technique called Double Dispatch,
* which helps to execute the proper method on an object without cumbersome conditionals.
* Instead of letting the client select a proper version of the method to call,
* how about we delegate this choice to objects we’re passing to the visitor as an argument?
* Since the objects know their own classes, they’ll be able to pick a proper method on the visitor less awkwardly.
* They “accept” a visitor and tell it what visiting method should be executed.
*
 */
public interface IExportVisitor
{
  String exportComposite(Composite composite);
  String exportSquare(Square square);
  String exportRectangle(Rectangle rectangle);
  String exportTriangle(Triangle triangle);
}
