package com.sodogan.patterns.dev.constraint;

public interface IConstraint<T>
{
    public abstract IConstraintResult<T> validateConstraint(T value);

}
