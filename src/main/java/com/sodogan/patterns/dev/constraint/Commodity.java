package com.sodogan.patterns.dev.constraint;

public class Commodity
{

    private static final double DEFAULT_MINIMUM_PRICE = 10.99d ;
    private static final double DEFAULT_MAXIMUM_PRICE    = 20.99d ;
    private double minimumPrice                       = DEFAULT_MINIMUM_PRICE;
    private double maximumPrice = DEFAULT_MAXIMUM_PRICE;


    public Commodity(int minimumPrice, int maximumPrice)
    {
//        validate here
        this.maximumPrice = maximumPrice;
        this.minimumPrice = minimumPrice;
    }


    public double getMinimumPrice()
    {
        return minimumPrice;
    }

    public double getMaximumPrice()
    {
        return maximumPrice;
    }


    public void getCommodityInfo()
    {
        System.out.println(this.toString());
    }

    @Override
    public String toString()
    {
        return "Commodity{" +
                "minimumPrice=" + minimumPrice +
                ", maximumPrice=" + maximumPrice +
                '}';
    }


}
