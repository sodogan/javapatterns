package com.sodogan.patterns.dev.constraint;

public class IsAndDecorator extends  ConstraintDecorator<Commodity>
{
    public IsAndDecorator(IConstraint<Commodity> constraint)
    {
        super(constraint);

    }
    @Override
    public IConstraintResult<Commodity> validateConstraint(Commodity value)
    {
//        get the base
        IConstraintResult result = constraint.validateConstraint(value);


        if (value.getMaximumPrice() > 10.00d) {
            result.add("The max value is above 10 ");
        }

        return result;



    }
}


