package com.sodogan.patterns.dev.constraint;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public  class ConstraintResultList implements IConstraintResult<String>
{


    private List<String> result_list = new ArrayList<>();

    @Override
    public boolean add(String value)
    {
        return result_list.add(value);
    }

    @Override
    public boolean remove(String value)
    {
        return result_list.remove(value);
    }

    @Override
    public List<String> getResult_list(){
        return  result_list;
    }

    public void setResult_list(List<String> result_list)
    {
        this.result_list = result_list;
    }


    @NotNull
    @Override
    public Iterator<String> iterator()
    {
        return result_list.iterator();
    }


}
