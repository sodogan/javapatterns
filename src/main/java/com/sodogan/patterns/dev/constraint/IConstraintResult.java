package com.sodogan.patterns.dev.constraint;

import java.util.List;

public interface IConstraintResult<V> extends Iterable<V>
{

    public boolean add(V value);
    public boolean remove(V value);
    public List<V> getResult_list();
}
