package com.sodogan.patterns.dev.constraint;

public abstract class ConstraintDecorator<T> implements  IConstraint<T>
{
    protected   IConstraint<T> constraint;

    public ConstraintDecorator(IConstraint<T> constraint){
        this.constraint = constraint;
    }

//    @Override
//    public IConstraintResult<T> validateConstraint(T value)
//    {
//        return constraint.validateConstraint(value);
//    }
}
