package com.sodogan.patterns.dev.constraint;

public class ConstraintMediator
{
    private ConstraintMediator(){

    }

    public static  <T> IConstraintResult<T> executeConstraint(IConstraint<T> constraint, T anyObject)
    {

        return constraint.validateConstraint(anyObject);

    }


}
