package com.sodogan.patterns.dev.constraint;

import java.util.List;

public class IsMinimumPriceIRangeValid implements IConstraint<Commodity>
{
    @Override
    public IConstraintResult<Commodity> validateConstraint(Commodity value)
    {
        IConstraintResult result = new ConstraintResultList();

        if (value.getMinimumPrice() > 1.00d) {
            result.add("The value is above 1 ");
        }

        return result;
    }
}
