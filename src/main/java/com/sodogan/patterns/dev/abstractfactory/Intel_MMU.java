package com.sodogan.patterns.dev.abstractfactory;

public class Intel_MMU implements IMMU
{
    @Override
    public void display()
    {
        System.out.println("Inside the Intel MMU");
    }
}
