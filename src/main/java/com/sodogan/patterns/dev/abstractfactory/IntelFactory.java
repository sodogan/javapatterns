package com.sodogan.patterns.dev.abstractfactory;

public class IntelFactory implements  IFactory
{
    @Override
    public ICPU createCPU()
    {
        return new Intel_CPU();
    }

    @Override
    public IMMU createMMU()
    {
        return new Intel_MMU();
    }
}
