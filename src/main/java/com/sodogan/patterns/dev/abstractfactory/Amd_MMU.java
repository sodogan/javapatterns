package com.sodogan.patterns.dev.abstractfactory;

public class Amd_MMU implements IMMU
{
    //package-private access
    Amd_MMU(){

    }
    @Override
    public void display()
    {
        System.out.println("Inside the AMD MMU");
    }
}
