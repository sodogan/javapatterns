package com.sodogan.patterns.dev.abstractfactory;

/* Abstract Factory patterns: Is to create a family of related objects
* In this example the family is divided based on the Architecture then product
* Intel family or AMD family
* Two factories are created and client only needs to ask the Abstract factory to create the right factory!
*
* Sometimes creational patterns are competitors: there are cases when either Prototype or Abstract Factory could be used profitably.
* At other times they are complementary: Abstract Factory might store a set of Prototypes from which to clone and return product objects,
* Builder can use one of the other patterns to implement which components get built. Abstract Factory, Builder can use Singleton in their implementation.
Abstract Factory, Builder, and Prototype define a factory object that's responsible for knowing and creating the class of product objects,
* and make it a parameter of the system. Abstract Factory has the factory object producing objects of several classes.
* Builder has the factory object building a complex product incrementally using a correspondingly complex protocol.
* Prototype has the factory object (aka prototype) building a product by copying a prototype object.
Abstract Factory classes are often implemented with Factory Methods, but they can also be implemented using Prototype.
Abstract Factory can be used as an alternative to Facade to hide platform-specific classes.
Builder focuses on constructing a complex object step by step. Abstract Factory emphasizes a family of product objects (either simple or complex).
* Builder returns the product as a final step, but as far as the Abstract Factory is concerned, the product gets returned immediately.
Often, designs start out using Factory Method (less complicated, more customizable, subclasses proliferate) and evolve toward
*  Abstract Factory, Prototype, or Builder (more flexible, more complex) as the designer discovers where more flexibility is needed.
 */
public abstract class AbstractFactory
{
    //Main entry for the client to get which factory is responsible for the creation
    public static IFactory getFactory(ArchitectureType type)
    {
        switch (type) {
            case INTEL:
                return new IntelFactory();
            case AMD:
                return new AMDFactory();
            default:
                return null;
        }
    }

//    public abstract ICPU createCPU();
//
//    public abstract IMMU createMMU();
}
