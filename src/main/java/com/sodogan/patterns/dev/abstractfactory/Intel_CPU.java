package com.sodogan.patterns.dev.abstractfactory;

public class Intel_CPU implements ICPU
{

    private final CPUType _type;

//    Make the constructor package-private so only this package can access it
    Intel_CPU()
    {
        this._type = CPUType.INTEL;
    }

    @Override
    public CPUType get_cpu_type()
    {
        return _type;
    }


    @Override
    public void display()
    {
        System.out.println("Inside the Intel Cpu");
    }
}
