package com.sodogan.patterns.dev.abstractfactory;

public enum ArchitectureType
{
    AMD,
    INTEL
}

