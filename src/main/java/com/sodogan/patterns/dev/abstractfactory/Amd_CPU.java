package com.sodogan.patterns.dev.abstractfactory;

public class Amd_CPU implements ICPU
{

    private final CPUType _type;
//package-private access
    Amd_CPU()
    {

        this._type = CPUType.AMD;
    }

    @Override
    public CPUType get_cpu_type()
    {
        return _type;
    }

    @Override
    public void display()
    {
        System.out.println("Inside the AMD Cpu");
    }
}
