package com.sodogan.patterns.dev.abstractfactory;

public interface IMMU
{
    void display();

    enum MMUType
    {AMD, INTEL}
}
