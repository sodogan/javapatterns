package com.sodogan.patterns.dev.abstractfactory;

public interface IFactory
{
    public abstract ICPU createCPU();

    public abstract IMMU createMMU();

}
