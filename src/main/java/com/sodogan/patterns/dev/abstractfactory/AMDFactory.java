package com.sodogan.patterns.dev.abstractfactory;

public class AMDFactory implements  IFactory
{
    @Override
    public ICPU createCPU()
    {
        return new Amd_CPU();
    }

    @Override
    public IMMU createMMU()
    {
        return new Amd_MMU();
    }
}
