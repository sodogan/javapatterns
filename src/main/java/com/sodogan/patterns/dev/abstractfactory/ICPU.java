package com.sodogan.patterns.dev.abstractfactory;

public interface ICPU
{
    enum CPUType
    { AMD,
      INTEL
    }
    CPUType get_cpu_type();
    void display();


}
