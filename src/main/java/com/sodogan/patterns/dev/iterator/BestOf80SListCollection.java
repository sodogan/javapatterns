package com.sodogan.patterns.dev.iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BestOf80SListCollection<E> implements IGenericCollection<E>
{
    private  List<E> songList = new ArrayList<>();
    @Override
    public void add(E anySong) throws ArrayIndexOutOfBoundsException
    {
        songList.add(anySong);
    }

    @Override
    public boolean remove(int index)
    {
        if( index < 0 || index >= songList.size()){
            return  false;
        }
        else{
            songList.remove(index);
           return  true;
        }
    }


    @Override
    public int getSize()
    {
         return songList.size();
    }

    @Override
    public E[] toArray()
    {
        return (E[]) songList.toArray();
    }



    @Override
    public Iterator iterator()
    {
        return new GenericListIterator();
    }


    private class GenericListIterator implements java.util.Iterator
    {
        @Override
        public boolean hasNext()
        {
            return false;
        }

        @Override
        public Song next()
        {
            return null;
        }
    }
}
