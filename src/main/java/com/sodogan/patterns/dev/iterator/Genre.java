package com.sodogan.patterns.dev.iterator;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.util.Strings;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class Genre
{
    private String name;

    public static final  Genre                        ROCK;
    public static final  Genre                        BLUES;
    public static final  Genre                        JAZZ;
    public static final  Genre                        ALTERNATIVE;
    public static final  Genre                        METAL;
    private static final ConcurrentMap<String, Genre> GENRE_LIST = new ConcurrentHashMap<>(); // SUPPRESS CHECKSTYLE


    private Genre(final String name)
    {
        if (Strings.isEmpty(name)) {
            throw new IllegalArgumentException("Illegal null or empty Level name.");
        }

        this.name = name;
//        Add it to the list
        Genre returned = GENRE_LIST.putIfAbsent(name, this);
        if (returned != null)
            throw new IllegalStateException("Level " + name + " has already been defined.");
    }

    static {
        ROCK = new Genre("Rock");
        BLUES = new Genre("Blues");
        JAZZ = new Genre("Jazz");
        ALTERNATIVE = new Genre("Alternative");
        METAL = new Genre("Metal");
    }

    public static Genre[] values()
    {
        final Collection<Genre> values = GENRE_LIST.values();
        return values.toArray(new Genre[values.size()]);
    }

}
