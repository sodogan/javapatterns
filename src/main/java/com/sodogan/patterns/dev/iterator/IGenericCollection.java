package com.sodogan.patterns.dev.iterator;

import java.util.Iterator;

//Iterable interface is extended as to be able to use enhaneed for loop
// Otherwise there is already an iterator method supplied here!
public interface IGenericCollection<E> extends  Iterable<E>
{
    void  add(E anySong);
    boolean remove(int index);
    int getSize();
    Object[] toArray();

    //Get the iterator
    Iterator<E> iterator();


}
