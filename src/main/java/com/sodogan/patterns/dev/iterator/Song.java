package com.sodogan.patterns.dev.iterator;

public  class Song
{

    private String      singer;
    private String      songName;
    private Genre genre;

    public Song(String singer, String songName, Genre genre)
    {
        this.singer = singer;
        this.songName = songName;
        this.genre = genre;
    }


    public String getSongInfo()
    {
        return toString();
    }

    @Override
    public String toString()
    {
        return "Song{" +
                "singer='" + singer + '\'' +
                ", songName='" + songName + '\'' +
                ", genre=" + genre +
                '}';
    }
}
