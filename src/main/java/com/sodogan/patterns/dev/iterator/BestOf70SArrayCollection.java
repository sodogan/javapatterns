package com.sodogan.patterns.dev.iterator;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Iterator;

public class BestOf70SArrayCollection<E> implements IGenericCollection<E>
{
    public static final int GC_INITIAL_ARRAY_SIZE      = 1;
    public static final int GC_DEFAULT_GROWTH_CAPACITY = 2;

    private E[] songs = (E []) new Object[GC_INITIAL_ARRAY_SIZE];
    private int     currentIndex;

    @NotNull
    @Override
    public Iterator<E> iterator()
    {
        return new GenericArrayIterator();
    }


    //    @Override
//    public IGenericIterator iterator()
//    {
//        return new GenericArrayIterator();
//    }
    //Inner class is needed as we need to access the private fields of the Current Object!
    private class GenericArrayIterator implements Iterator<E>
    {
        private  int progress = 0;
        @Override
        public boolean hasNext()
        {
            return progress < getSize();
        }

        @Override
        public E next()
        {
            E song= songs[progress];
            progress++;
            return song;

        }
    }


    public void add(E anySong)
    {
        if (currentIndex == GC_INITIAL_ARRAY_SIZE) {
//  Need to grow the array
            songs = (E[]) grow(songs, 50);
        }
        songs[currentIndex] = anySong;
        currentIndex++;

    }

    @Override
    public boolean remove(int index)
    {
//        validate that index is valid
//         index should be less then the size
        if (getSize() == 0) {
            return false;
        }
        if (index < getSize()) {
         return  true;
        }
        return false;
    }

    @NotNull
    private E[] grow(E[] tobeCopied, int length)
    {
        return Arrays.copyOf(tobeCopied, length);
    }




    @Override
    public int getSize()
    {
        int size = 0;
        for (E song : songs) {
            if (song != null) {
                size++;
            }
        }
        return size;
    }

    @Override
    public Object[] toArray()
    {
        return Arrays.copyOf(songs, getSize());
    }

//    @Override
//    public <E> E[] toArray()
//    {
//        int realSize = getSize();
//        E[] result = (E[]) new Object[realSize];
//        for (int index = 0; index < realSize; index++) {
//            result[index] = (E) songs[index];
//        }
//
//        return result;
//    }

    public E[] removeAndShift(int index)
    {
        int movingIndex = index + 1;
        int currentIndex = index;
        int limit = getSize();
        songs[index] = null;
//        shift the values
        while (movingIndex <= limit)
        {
            songs[currentIndex] = songs[movingIndex];
            movingIndex++;
            currentIndex++;
        }

        return songs;
    }


}
