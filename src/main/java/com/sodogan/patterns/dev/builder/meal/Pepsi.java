package com.sodogan.patterns.dev.builder.meal;

public class Pepsi extends  Drink
{
    public Pepsi(IPacking packing)
    {
        super(packing,2.2d);
    }
}
