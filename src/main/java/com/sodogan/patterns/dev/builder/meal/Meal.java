package com.sodogan.patterns.dev.builder.meal;

import com.sun.source.tree.BreakTree;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public abstract class Meal
{
    protected List<MealItem> mealItems= new ArrayList<>();
    private double totalCost;

    public double getTotalCost()
    {
     /**OLD Imperative way
        double total = 0d;
        for (MealItem item : mealItems) {
            total = total + item.getCost();
        }
      */


        double total = mealItems.stream()
                .filter(i -> i.getCost() > 0.0d)
                .mapToDouble(value ->value.getCost())
                .sum();


        return total;
        
    }

    public Meal(List<MealItem> mealItems)
    {
        this.mealItems = mealItems;
    }

    public List<MealItem> getMealItems()
    {
        return mealItems;
    }


}
