package com.sodogan.patterns.dev.builder;

public enum HouseType
{
    WOODEN,
    GLASS,
    APARTMENT
}
