package com.sodogan.patterns.dev.builder.meal;

public abstract class Drink extends  MealItem
{
    public Drink(IPacking packing,double cost)
    {
        super(packing,cost);
    }
}
