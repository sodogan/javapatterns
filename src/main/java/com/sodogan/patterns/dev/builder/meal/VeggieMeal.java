package com.sodogan.patterns.dev.builder.meal;

import java.util.ArrayList;
import java.util.List;

public class VeggieMeal extends Meal
{

    public VeggieMeal(List<MealItem> mealItems)
    {
        super(mealItems);
    }
}
