package com.sodogan.patterns.dev.builder.meal;

public class VeggieBurger  extends  Burger
{
    public VeggieBurger()
    {

        super(new WrappingPacking(),6.7d);
    }
}
