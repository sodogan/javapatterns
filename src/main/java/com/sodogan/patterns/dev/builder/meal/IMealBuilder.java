package com.sodogan.patterns.dev.builder.meal;

/*
 *  Meal Builder be regarded as the Restaurant crew!
 *  Works under the supervision of the Director(Cashier) what to construct each step!
 */
public interface IMealBuilder
{

    public abstract void  addBurger();
    public abstract void  addDrink();
    public abstract void  pack();
    public abstract Meal  getMeal();


}
