package com.sodogan.patterns.dev.builder;
/*
 * The Builder interface declares product construction steps that are common to all types of builders.
 */
public interface IHouseBuilder
{
    public abstract IHouseBuilder buildFoundation();
    public abstract IHouseBuilder buildPillars();
    public abstract IHouseBuilder buildWindows();
    public abstract IHouseBuilder buildWalls();
    public abstract IHouseBuilder buildRoof();
    public abstract House         getHouse();
}
