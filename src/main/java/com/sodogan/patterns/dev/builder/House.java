package com.sodogan.patterns.dev.builder;

/*
 * Use the Builder pattern to get rid of a “telescopic constructor”.
 * Say you have a constructor with ten optional parameters. Calling such a beast is very inconvenient; therefore,
 * you overload the constructor and create several shorter versions with fewer parameters.
 * These constructors still refer to the main one, passing some default values into any omitted parameters.
 */
public abstract class House implements  HousePlan
{

    protected IFoundation foundation;
    protected IWindows windows;
    protected IWalls walls;
    protected IPillars pillars;
    protected IRoof roof;
    protected HouseType _houseType;



    public void setFoundation(IFoundation foundation)
    {
        this.foundation = foundation;
    }

    public void setWindows(IWindows windows)
    {
        this.windows = windows;
    }

    public void setWalls(IWalls walls)
    {
        this.walls = walls;
    }

    public void setPillars(IPillars pillars)
    {
        this.pillars = pillars;
    }

    public void setRoof(IRoof roof)
    {
        this.roof = roof;
    }

    public void set_houseType(HouseType _houseType)
    {
        this._houseType = _houseType;
    }


}




