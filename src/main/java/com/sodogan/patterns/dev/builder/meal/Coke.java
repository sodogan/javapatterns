package com.sodogan.patterns.dev.builder.meal;

public class Coke extends   Drink
{
    public Coke()
    {

        super(new BottlePacking(),1.5d);
    }
}
