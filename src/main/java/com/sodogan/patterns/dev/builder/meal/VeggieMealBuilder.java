package com.sodogan.patterns.dev.builder.meal;

import java.util.ArrayList;
import java.util.List;

public class VeggieMealBuilder implements IMealBuilder
{

    private List<MealItem> mealItems = new ArrayList<>();
    private IPacking packing;

    @Override
    public void addBurger()
    {
        mealItems.add(new VeggieBurger());
    }

    @Override
    public void addDrink()
    {
        mealItems.add(new Coke());

    }

    @Override
    public void pack()
    {
        packing = new BottlePacking();
    }


    @Override
    public Meal getMeal()
    {
        return new VeggieMeal(mealItems);
    }
}
