package com.sodogan.patterns.dev.builder.meal;

public abstract class Burger extends  MealItem
{
    public Burger(IPacking packing,double cost)
    {
        super(packing,cost);
    }
}
