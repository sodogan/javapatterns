package com.sodogan.patterns.dev.builder;

/*
 * The Director class defines the order in which to call construction steps,
 * so you can create and reuse specific configurations of products.
 * Imagine a complex object that requires laborious, step-by-step initialization of many fields and nested objects.
 * Such initialization code is usually buried inside a monstrous constructor with lots of parameters.
 * Or even worse: scattered all over the client code.
 */

public class HouseDirector
{

    protected IHouseBuilder _builder;

    public HouseDirector(IHouseBuilder _builder)
    {
        this._builder = _builder;
    }


    public House build()
    {
        _builder.buildFoundation()
                .buildWalls()
                .buildPillars()
                .buildWindows()
                .buildRoof();
        return _builder.getHouse();
    }

    public void change_builder(IHouseBuilder _builder)
    {
        this._builder = _builder;
    }

}


