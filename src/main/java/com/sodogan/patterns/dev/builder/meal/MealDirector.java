package com.sodogan.patterns.dev.builder.meal;

/*
*  Meal Director can be regarded as the Cashier!
*  Cashier will tell the restaurant crew (MealBuilder) what to construct each step!
 */
public class MealDirector
{
    private  IMealBuilder mealBuilder;

    public MealDirector(IMealBuilder mealBuilder)
    {
        this.mealBuilder = mealBuilder;
    }


    public  Meal buildMeal(){
        mealBuilder.addBurger();
        mealBuilder.addDrink();
        mealBuilder.pack();
        return mealBuilder.getMeal();

    }
}
