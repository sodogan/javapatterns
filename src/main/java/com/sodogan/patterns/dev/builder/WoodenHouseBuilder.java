package com.sodogan.patterns.dev.builder;
/*
* Concrete Builders provide different implementations of the construction steps.
* Concrete builders may produce products that don’t follow the common interface.
 * When the object creation is complex -formed of many other objects
 * Factory pattern would not be enough to create an object
 * And creation needs to be step by step
 * Check Director-Builder design template
 */
public  class WoodenHouseBuilder implements  IHouseBuilder
{

    private  IFoundation foundation;
    private  IWalls walls;
    private  IWindows windows;
    private  IPillars pillars;
    private  IRoof roof;


    @Override
    public IHouseBuilder buildFoundation()
    {
        this.foundation = new BasicFoundation();
        return this;
    }

    @Override
    public IHouseBuilder buildPillars()
    {
        this.pillars = new WoodenPillars();
        return this;
    }

    @Override
    public IHouseBuilder buildWindows()
    {
        this.windows = new WoodenWindows();
        return this;
    }

    @Override
    public IHouseBuilder buildWalls()
    {
        this.walls = new WoodenWalls();
        return this;
    }

    @Override
    public IHouseBuilder buildRoof()
    {
        this.roof = new WoodenRoof();
        return this;
    }

    public House getHouse()
    {
        House house = new WoodenHouse();
        house.setFoundation(this.foundation);
        house.setWindows(this.windows);
        house.setWalls(this.walls);
        house.setPillars(this.pillars);
        house.setRoof(this.roof);
        return  house;
    }


}
