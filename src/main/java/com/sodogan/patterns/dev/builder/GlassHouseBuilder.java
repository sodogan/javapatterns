package com.sodogan.patterns.dev.builder;

/*
 * When the object creation is complex -formed of many other objects
 * Factory pattern would not be enough to create an object
 * And creation needs to be step by step
 * Check Director-Builder design template
 */
public class GlassHouseBuilder implements IHouseBuilder
{

    private IFoundation foundation;
    private IWalls walls;
    private IWindows windows;
    private IPillars pillars;
    private IRoof roof;


    @Override
    public IHouseBuilder buildFoundation()
    {
        this.foundation = new ConcreteFoundation();
        return this;
    }

    @Override
    public IHouseBuilder buildPillars()
    {
        this.pillars = new GlassPillars();
        return this;
    }

    @Override
    public IHouseBuilder buildWindows()
    {
        this.windows = new GlassWindows();
        return this;
    }

    @Override
    public IHouseBuilder buildWalls()
    {
        this.walls = new GlassWalls();
        return this;
    }

    @Override
    public IHouseBuilder buildRoof()
    {
        this.roof = new GlassRoof();
        return this;
    }

    public House getHouse()
    {
        House house = new GlassHouse();
        house.setFoundation(this.foundation);
        house.setWindows(this.windows);
        house.setWalls(this.walls);
        house.setPillars(this.pillars);
        house.setRoof(this.roof);
        return  house;

    }


}
