package com.sodogan.patterns.dev.builder;

public class DifferentHouseDirector extends  HouseDirector
{
    public DifferentHouseDirector(IHouseBuilder _builder)
    {
        super(_builder);
    }

    @Override
    public House build()
    {
        _builder.buildFoundation()
                .buildWalls()
                .buildWindows()
                .buildRoof()
                .buildPillars();
        return _builder.getHouse();
    }
}
