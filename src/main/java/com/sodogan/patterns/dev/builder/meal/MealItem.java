package com.sodogan.patterns.dev.builder.meal;

public abstract  class MealItem
{


    protected  IPacking packing;
    protected  double   cost;


    public MealItem(IPacking packing, double cost)
    {
        this.packing = packing;
        this.cost = cost;
    }


    public double getCost()
    {
        return cost;
    }

    public void setCost(double cost)
    {
        this.cost = cost;
    }




    public IPacking getPacking()
    {
        return this.packing;
    }

    public void setPacking(IPacking packing)
    {
        this.packing = packing;
    }




}
