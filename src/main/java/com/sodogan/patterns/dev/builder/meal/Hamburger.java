package com.sodogan.patterns.dev.builder.meal;

public class Hamburger extends   Burger
{
    public Hamburger(IPacking packing)
    {
        super(packing,5.6d);
    }
}
