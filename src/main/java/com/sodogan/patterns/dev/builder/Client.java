package com.sodogan.patterns.dev.builder;

/*
 * How is the Client going to interact with the Builder Pattern
 */
public class Client
{
    public  static  void  main(String args[]){


    }

    //How to build the Wooden House
    private  static  House createWoodenHouse(){
        IHouseBuilder woodenHouseBuilder = new WoodenHouseBuilder();
        HouseDirector houseDirector = new HouseDirector(woodenHouseBuilder);
        return houseDirector.build();
    }


    //How to build the Wooden House
    private  static  House createGlassHouse(){
        IHouseBuilder glassHouseBuilder = new GlassHouseBuilder();
        HouseDirector houseDirector = new HouseDirector(glassHouseBuilder);
        return houseDirector.build();
    }



}
