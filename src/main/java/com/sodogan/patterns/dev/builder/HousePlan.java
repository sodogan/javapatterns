package com.sodogan.patterns.dev.builder;

public interface HousePlan
{
    public abstract void setFoundation(IFoundation foundation);

    public abstract void setWindows(IWindows windows);

    public abstract void setWalls(IWalls walls);

    public abstract void setPillars(IPillars pillars);

    public abstract void setRoof(IRoof roof);

    public abstract void set_houseType(HouseType _houseType);

}
