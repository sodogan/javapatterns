package com.sodogan.patterns.dev.state;
/*
 * Just a dummy moderator- that will always accept to be published!
 */
interface IModerator
{
    public static final IModerator NOOP = new IModerator()
    {
    };

    default boolean isAccepted()
    {
        return true;
    }
}
