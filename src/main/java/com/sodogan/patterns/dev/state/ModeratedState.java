package com.sodogan.patterns.dev.state;

public class ModeratedState implements  State
{
    @Override
    public void publish(WordDocumentContext context)
    {
//        accept all times
        context.changeState(new PublishedState());
    }
}
