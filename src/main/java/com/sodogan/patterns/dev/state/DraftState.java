package com.sodogan.patterns.dev.state;

public class DraftState implements  State
{

    @Override
    public void publish(WordDocumentContext context)
    {
//           change the state
        context.changeState(new ModeratedState());
    }
}
