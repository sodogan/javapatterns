package com.sodogan.patterns.dev.state;

import java.time.LocalDateTime;
import java.util.Date;

public class WordDocumentContext
{

    private String author;
    private String version;
    private State  state;


    public WordDocumentContext(String author, String version)
    {
        this.author = author;
        this.version = version;
        this.state = new DraftState();
    }

    public void publish()
    {
//delegated to the state
        state.publish(this);
    }

    public void changeState(State state)
    {
        this.state = state;
    }
    public State getState(){
        return  state;
    }


//    Before publishing-Document needs to be moderated and then accepted so can be published


}
