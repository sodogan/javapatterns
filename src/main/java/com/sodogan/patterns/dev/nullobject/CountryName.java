package com.sodogan.patterns.dev.nullobject;

public enum CountryName
{
    AUSTRALIA,
    DENMARK,
    THAILAND,
    NULL
}
