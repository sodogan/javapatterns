package com.sodogan.patterns.dev.nullobject;

/*
 *  Represents a Null object instead of returning null we return this object
 *  Null Object should be singleton as we retun the same null object
 *
 * */
public class NullTax implements ITax
{
    private  static  NullTax _self = new NullTax();

    private NullTax(){}
    @Override
    public double calculate_tax(double salary)
    {
        return 0;
    }

    public  static  NullTax get_instance(){
        return _self;
    }

    @Override
    public ITax clone()
    {
        return _self;
    }
}
