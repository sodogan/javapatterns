package com.sodogan.patterns.dev.nullobject;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
/*Now we get to the Null Object pattern's role. Rather than using null to represent an empty list,
* we will create a NullList class to represent the empty list. Notice that it knows exactly what to do.
* Access to the head was intentionally left out of the abstract list because the NullList would be unable to fulfill that interface.
*/
public class TaxFactory
{
    private static Map<CountryName, ITax> taxPerCountrylist = new ConcurrentHashMap<>();

    static {
        taxPerCountrylist.put(CountryName.AUSTRALIA, new AussieTax());
        taxPerCountrylist.put(CountryName.DENMARK, new DanishTax());
        taxPerCountrylist.put(CountryName.NULL, NullTax.get_instance());
    }


    /* Sometimes, it is possible to use a special Null Object to encapsulate the absence of an instance
     *  by providing an alternative that behaves in a suitably passive way. Basically, instead of using a null value,
     *  create an object which has no impact on anything.
     *  The object will also be able to provide information on why a non-standard value was returned,
     *  whereas a null value cannot.
     */
    public static ITax getTaxByCountry(CountryName country)
    {
       ITax tax = null;

        if (taxPerCountrylist.containsKey(country)) {
            tax = taxPerCountrylist.get(country).clone();
        }
// This null might cause errors!
// Replace it with a special NullTax object which does not modify the value of price.
//        else
//            return null;
        else{
            tax = taxPerCountrylist.get(CountryName.NULL).clone();
        }
       return tax;



    }


}