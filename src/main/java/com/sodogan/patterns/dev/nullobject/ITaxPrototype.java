package com.sodogan.patterns.dev.nullobject;

public interface ITaxPrototype
{
    ITax clone();
}
