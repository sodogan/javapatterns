package com.sodogan.patterns.dev.nullobject;
/*
* A Null Object can be a special case of the Strategy pattern. Strategy specifies several ConcreteStrategy classes
* as different approaches for accomplishing a task. If one of those approaches is to consistently do nothing,
* that ConcreteStrategy is a NullObject. F
* or example, a Controller is a View's Strategy for handling input, and NoController is the Strategy that ignores all input.
*/
public interface ITax extends  ITaxPrototype
{
    static final double DANISH_TAX_RATE = 0.4;
    static final double AUSSIE_TAX_RATE = 0.5;

    public  double calculate_tax(double salary);
}
