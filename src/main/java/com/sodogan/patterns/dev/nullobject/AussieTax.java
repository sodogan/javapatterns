package com.sodogan.patterns.dev.nullobject;

public class AussieTax implements ITax
{

    @Override
    public double calculate_tax(double salary)
    {
        return salary * ITax.AUSSIE_TAX_RATE;
    }

    @Override
    public ITax clone()
    {
        return new AussieTax();
    }
}
