package com.sodogan.patterns.dev.nullobject;

public class DanishTax implements ITax
{



    @Override
    public double calculate_tax(double salary)
    {
        return salary * ITax.DANISH_TAX_RATE;
    }

    @Override
    public ITax clone()
    {
        return new DanishTax();
    }
}
