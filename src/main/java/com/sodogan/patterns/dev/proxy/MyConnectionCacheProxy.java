package com.sodogan.patterns.dev.proxy;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

public class MyConnectionCacheProxy implements IMyConnection
{
    private String        connectionURL;
    private Properties    properties;
    private IMyConnection connection;

    private Map<String, Connection> cache = new ConcurrentHashMap<>();

    //
    public MyConnectionCacheProxy(String connectionURL, Properties properties)
    {
        this.connectionURL = connectionURL;
        this.properties = properties;
    }


    @Override
    public Connection connect() throws SQLException
    {
//        check if its in the cache already
        if (cache.containsKey(connectionURL)) {
            return cache.get(connectionURL);
        } else {
            connection = new MyConnection(connectionURL, properties);
//            add it to the cache
            cache.put(connectionURL, connection.getConnection());
            return connection.getConnection();
        }
    }

    @Override
    public void close() throws SQLException
    {
        if(validate())
            connection.close();
    }

    @Override
    public boolean validate() throws SQLException
    {
        return connection.validate();
    }

    @Override
    public Connection getConnection()
    {
        return connection.getConnection();
    }
}
