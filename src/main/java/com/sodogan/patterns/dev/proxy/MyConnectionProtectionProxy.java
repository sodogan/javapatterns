package com.sodogan.patterns.dev.proxy;

import javax.naming.AuthenticationException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

public class MyConnectionProtectionProxy implements IMyConnection
{
    private MyConnection myConnection;
    private String connectionURL;
    private Properties properties;

    private Map<String,String> userPasswords = new ConcurrentHashMap<>() ;

    public MyConnectionProtectionProxy(String connectionURL, Properties properties)
    {
        this.connectionURL = connectionURL;
        this.properties = properties;
//          allowed users are loaded
        setAllowedUsers();

    }

    private void setAllowedUsers()
    {
        userPasswords.put("user","root");
        userPasswords.put("password","Taksim12");
    }

    @Override
    public Connection connect() throws SQLException, AuthenticationException
    {
//protection proxy will authenticate first then foward it to the
        if (validateUser(properties)) {
            this.myConnection = new MyConnection(connectionURL, properties);
            return myConnection.connect();
        }
        throw new AuthenticationException("Authentication has failed");

    }

    private boolean validateUser(Properties properties)
    {
        System.out.println("inside validate"+properties.getProperty("user"));

        boolean flag = userPasswords.containsValue(properties.getProperty("user")) &&
                       userPasswords.containsValue(properties.getProperty("password"));
        return  flag;
    }

    @Override
    public void close() throws SQLException
    {
        myConnection.close();
    }

    @Override
    public boolean validate() throws SQLException
    {
        return myConnection.validate();
    }

    @Override
    public Connection getConnection()
    {
        return myConnection.getConnection();
    }
}
