package com.sodogan.patterns.dev.proxy;

import javax.naming.AuthenticationException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public interface IMyConnection
{
    public Connection connect() throws SQLException, AuthenticationException;
    public void close() throws SQLException;
    public boolean validate() throws SQLException;
    public Connection getConnection() ;

}
