package com.sodogan.patterns.dev.proxy;

import java.sql.Connection;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;


public class MyDependencyInjector
{
    public static <T> void injectConnection(Class<T> classToMock)
    {
        T mockConnection = mock(classToMock);
        MyDependencyLookupFactory.injectConnection((java.sql.Connection) mockConnection);
//        return  mockConnection;
    }

}
