package com.sodogan.patterns.dev.proxy;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class MyConnection implements IMyConnection
{
    private Connection connection;
    private String     connectionURL;
    private Properties properties;

    public MyConnection(String connectionURL, Properties properties) throws SQLException
    {
        this.connectionURL = connectionURL;
        this.properties = properties;
//        do connect here-No catch here
        connection = connect();
        System.out.println("Successfully connected to MySQL database test");

    }

    public Connection connect() throws SQLException
    {
       return MyDependencyLookupFactory.getNewConnection(connectionURL,properties);
    }

    @Override
    public void close() throws SQLException
    {
        if (connection.isClosed() == false) connection.close();
    }

    @Override
    public boolean validate() throws SQLException
    {
        if (connection.isClosed())
            return false;
        else
            return true;
    }

    @Override
    public Connection getConnection()
    {
        return this.connection;
    }
}
