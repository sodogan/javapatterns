package com.sodogan.patterns.dev.proxy;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class MyDependencyLookupFactory
{

    private static Connection connection;

    public static Connection getNewConnection(String connectionURL, Properties properties) throws SQLException
    {
        if (connection == null)
            connection = DriverManager.getConnection(connectionURL, properties);
      return  connection;
    }

    static void injectConnection(Connection mockConnection){
        connection = mockConnection;
    }


}
