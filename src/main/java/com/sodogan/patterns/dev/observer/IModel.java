package com.sodogan.patterns.dev.observer;

/*
* Represents ths Model in MVC which is the data
* Its the subject in the Observer pattern-the centre of the change
* Whenever the subject changes the listeners needs to be notified!
* There are two approaches: PULL Model or PUSH Model
 */
import java.util.List;
public interface IModel
{

    void addListener(IView observer);
    void removeListener(IView observer);
    void notifyListeners();
    List<IView> getListeners();
    void editData(String data);
    boolean isChanged();
}
