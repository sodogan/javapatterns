package com.sodogan.patterns.dev.observer;


public class UI5View  implements IView
{
    private IModel subject;


    public UI5View(IModel subject){
        this.subject = subject;
        subject.addListener(this);
    }
    @Override
    public void update(IModel subject)
    {
      subject = subject;
    }

    @Override
    public IModel getModel()
    {
        return subject;
    }

}
