package com.sodogan.patterns.dev.observer;

import java.util.ArrayList;
import java.util.List;

public class JSONModel implements IModel
{

    // The core data is storedin json format!
    private String jsonData;

    private List<IView> listOfListeners = new ArrayList<>();
    private boolean isModified = false;

    public JSONModel(String jsonData)
    {
     this.jsonData = jsonData;
    }


    @Override
    public void addListener(IView observer)
    {
        listOfListeners.add(observer);
    }

    @Override
    public void removeListener(IView observer)
    {
        listOfListeners.remove(observer);

    }

    @Override
    public List<IView> getListeners()
    {
        return listOfListeners;
    }

    @Override
    public void notifyListeners()
    {
        if (isModified == true) {
            getListeners().forEach(
                    (IView observer) -> observer.update(this));
        }
    }


    @Override
    public void editData(String jsonData)
    {
        if (!this.jsonData.equalsIgnoreCase(jsonData)){
            isModified = true;
            //Notify all the listeners!
            notifyListeners();
        }

    }

    @Override
    public boolean isChanged()
    {
        return isModified;
    }

    public String getJsonData()
    {
        return jsonData;
    }



}
