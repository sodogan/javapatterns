package com.sodogan.patterns.dev.observer;

public class HTMLView implements IView
{
    private IModel subject;


    public HTMLView(IModel subject){
        this.subject = subject;
        subject.addListener(this);
    }
    @Override
    public void update(IModel subject)
    {
//     subject.
    }

    @Override
    public IModel getModel()
    {
        return subject;
    }

}
