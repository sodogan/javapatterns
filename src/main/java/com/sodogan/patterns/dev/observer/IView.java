package com.sodogan.patterns.dev.observer;

public interface IView
{
    void update(IModel subject);
    IModel getModel();

}
