package com.sodogan.patterns.dev.template;

public class WoodenHouse extends House
{

    @Override
    public void buildWalls()
    {
        this.walls = new WoodenWalls();
    }

    @Override
    public void buildPillars()
    {
        this.pillars = new WoodenPillars();

    }

    @Override
    public void buildWindows()
    {
        this.windows = new WoodenWindows();

    }

    @Override
    public void buildRoof()
    {
        this.roof = new WoodenRoof();
    }
}
