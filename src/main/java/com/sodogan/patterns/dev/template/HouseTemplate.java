package com.sodogan.patterns.dev.template;

/*
* Define the skeleton of an algorithm in an operation, deferring some steps to client subclasses.
* Template Method lets subclasses redefine certain steps of an algorithm without changing the algorithm's structure.
* Base class declares algorithm 'placeholders', and derived classes implement the placeholders.
 */
public interface HouseTemplate
{
    public abstract void  buildFoundation();
    public abstract void  buildWalls();
    public abstract void  buildPillars();
    public abstract void  buildWindows();
    public abstract void  buildRoof();
}
