package com.sodogan.patterns.dev.template;

public enum HouseType
{
    WOODEN,
    GLASS,
    APARTMENT
}
