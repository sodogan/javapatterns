package com.sodogan.patterns.dev.template;
/*
* Template Method is used prominently in frameworks. Each framework implements the invariant pieces of a domain's architecture,
* and defines "placeholders" for all necessary or interesting client customization options.
* In so doing, the framework becomes the "center of the universe",
* and the client customizations are simply "the third rock from the sun".
* This inverted control structure has been affectionately labelled "the Hollywood principle" - "don't call us, we'll call you".
 */
public abstract class House implements HouseTemplate
{

    protected Foundation foundation;
    protected Windows windows;
    protected Walls walls;
    protected Pillars pillars;
    protected Roof roof;

    public House()
    {

    }

    // 1. Standardize the skeleton of an algorithm in a "template" method
    public House buildHouse()
    {
        buildFoundation();
        buildWalls();
        buildPillars();
        buildWindows();
        buildRoof();
        return this;
    }

    //    Common to all of the Houses have the same Foundation
    @Override
    public void buildFoundation()
    {
        this.foundation = new BasicFoundation();
    }


    public Windows getWindows()
    {
        return windows;
    }

    public Walls getWalls()
    {
        return walls;
    }

    public Pillars getPillars()
    {
        return pillars;
    }

    public Roof getRoof()
    {
        return roof;
    }

    public Foundation getFoundation()
    {
        return foundation;
    }

    @Override
    public String toString()
    {
        return "House{" +
                "foundation=" + foundation +
                ", windows=" + windows +
                ", walls=" + walls +
                ", pillars=" + pillars +
                ", roof=" + roof +
                '}';
    }
}




