package com.sodogan.patterns.dev.template;

public class GlassHouse extends House
{

    @Override
    public void buildWalls()
    {
     this.walls = new GlassWalls();
    }

    @Override
    public void buildPillars()
    {
        this.pillars = new GlassPillars();

    }

    @Override
    public void buildWindows()
    {
        this.windows = new GlassWindows();

    }

    @Override
    public void buildRoof()
    {
        this.roof = new GlassRoof();

    }
}
