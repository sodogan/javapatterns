package com.sodogan.patterns.dev.template;

public abstract class HouseFactory
{

    public static House build(HouseType houseType)
    {
        House houseBuilt = null;
        switch (houseType) {
            case GLASS:
                houseBuilt = new GlassHouse();
                break;
            case WOODEN:
                houseBuilt = new WoodenHouse();
                break;
//           case APARTMENT:
//               houseBuilt = new ApartmentHouse();
//               break;
            default:
                throw new IllegalStateException("Unexpected value: " + houseType);
        }

        return houseBuilt.buildHouse();

    }


}
