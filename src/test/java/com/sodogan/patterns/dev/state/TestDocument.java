package com.sodogan.patterns.dev.state;

import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestDocument
{

    @Test
    @DisplayName("Testing document object")
    public void howisDocumentCreated()
    {

        WordDocumentContext context = new WordDocumentContext("solen", "1.0");
//    when
        context.publish();
//Then state should be moved from Draft to Moderated
       State actual =  context.getState();
       assertTrue(actual instanceof  ModeratedState,"The state should move to Draft");
//        Assertions.fail("Failed");

//    when
        context.publish();
//Then state should be moved from Draft to Moderated
         actual =  context.getState();
        assertTrue(actual instanceof  PublishedState,"The state should move to Draft");

    }


}
