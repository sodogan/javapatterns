package com.sodogan.patterns.dev.abstractfactory;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestAbstractFactory
{
    private IFactory intelFactory;
    private IFactory amdFactory;

    @BeforeEach
    public void setup()
    {
        intelFactory =  AbstractFactory.getFactory(ArchitectureType.INTEL);
        amdFactory = AbstractFactory.getFactory(ArchitectureType.AMD);
     }


    @Test
    @DisplayName("Can create CPU via Intel Factory")
    public void testAbstract()
    {
//        when
      ICPU intel =   intelFactory.createCPU();
//      Then
      ICPU.CPUType actual = intel.get_cpu_type();
      assertEquals(ICPU.CPUType.INTEL,actual,"Intel should be created");

//         fail("Always start with a fail test");

        
    }




}
