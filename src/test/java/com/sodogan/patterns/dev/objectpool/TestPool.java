package com.sodogan.patterns.dev.objectpool;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.*;
import java.util.Enumeration;
import java.util.Hashtable;

public class TestPool
{

    @Test
    @DisplayName("Test iteration")
    public void testIteration(){

        Hashtable<Integer,String> hashList = new Hashtable<>();
//        add some items
        hashList.put(1,"Logger");
        hashList.put(2,"Driver");

        Enumeration<Integer> e = hashList.keys();

        while (e.hasMoreElements()) {
            int key = e.nextElement();
             String value= hashList.get(key);
            System.out.println(value);
        }
    }

    @Test
    @DisplayName("Testing JDB Connection")
    public  void testJDBCConnection() throws SQLException
    {
        String connectionUrl = "jdbc:mysql://localhost:3306/world";
        AbstractConnectionPool jdbcConnectionPool = new JDBCConnectionPool(connectionUrl);
        Connection connection = jdbcConnectionPool.checkOut();
        jdbcConnectionPool.checkIn(connection);
//        Connection connection = jdbcConnectionPool.create();
//        assertTrue(connection!= null);
//
//        assertEquals(true, jdbcConnectionPool.validate(connection));

    }


}
