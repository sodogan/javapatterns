package com.sodogan.patterns.dev.prototype;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestColorRegistry
{

	
	@Test
	@DisplayName("Is clone same as real")
	public void isCloneSameAsReal() {
//		Given -WHEN
		RedColor _redClone = (RedColor) ColorRegistry.getColor(IColor.Colors.RED);
//		Then
		assertNotNull(_redClone);
		BlueColor _blueClone = (BlueColor) ColorRegistry.getColor(IColor.Colors.BLUE);

//		RedColor _redActual = new RedColor();
//		Then
//		assertTrue(_redActual.equals(_redClone));
//		assertEquals(_redClone,_redActual,"Clone should be same as real object");

//		fail("Start with  a fail");
	}
	
}
