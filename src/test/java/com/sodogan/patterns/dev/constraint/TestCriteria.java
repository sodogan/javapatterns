package com.sodogan.patterns.dev.constraint;

import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TestCriteria
{


    @Test
    @DisplayName("Testing the citeria")
    public void testCriteria()
    {
//        Given
        Commodity commodity = new Commodity(10, 29);
        IConstraint<Commodity> constraint = new IsAndDecorator(new IsMinimumPriceIRangeValid());
        IConstraintResult<Commodity> result = ConstraintMediator.executeConstraint(constraint, commodity);
        List<Commodity> resultList = result.getResult_list();

        assertTrue(resultList.size() == 2);
//          List<String> actual = abstractConstraint.getDescription();
//          List<String> expected   = new ArrayList<>();
//          expected.add("The price is not in the range");
//
//          assertLinesMatch(expected,actual);

//        fail("Always start with a failure");

    }

}
