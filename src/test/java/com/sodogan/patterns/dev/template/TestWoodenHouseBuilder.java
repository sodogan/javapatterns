package com.sodogan.patterns.dev.template;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


import java.awt.*;
import java.io.FileOutputStream;

public class TestWoodenHouseBuilder
{
    @Test
    @DisplayName("Test creation of Wooden House")
    public void howtoCreateWoodenHouse()
    {
        //      fail("Not yet");
        House woodenHouse = HouseFactory.build(HouseType.WOODEN);
        System.out.println(woodenHouse.toString());
        assertNotNull(woodenHouse);
        assertTrue(woodenHouse.getWalls() instanceof  WoodenWalls);
        assertTrue(woodenHouse.getWindows() instanceof  WoodenWindows);
        assertTrue(woodenHouse.getRoof() instanceof  WoodenRoof);
        assertTrue(woodenHouse.getPillars() instanceof  WoodenPillars);

    }
    @Test
    @DisplayName("Test creation of Glass House")
    public void howtoCreateGlassHouse()
    {
//              fail("Not yet");

        House glassHouse = HouseFactory.build(HouseType.GLASS);
        System.out.println(glassHouse.toString());
        assertNotNull(glassHouse);
        assertTrue(glassHouse.getWalls() instanceof  GlassWalls);
        assertTrue(glassHouse.getWindows() instanceof  GlassWindows);
        assertTrue(glassHouse.getRoof() instanceof  GlassRoof);
        assertTrue(glassHouse.getPillars() instanceof  GlassPillars);

    }





}
