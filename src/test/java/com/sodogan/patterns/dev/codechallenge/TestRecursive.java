package com.sodogan.patterns.dev.codechallenge;

import static org.junit.jupiter.api.Assertions.*;

import com.sodogan.patterns.dev.objectpool.ObjectPool;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Executable;
import java.util.Objects;
import java.util.Stack;

public class TestRecursive
{

    private  Recursiver recursiver;


    @BeforeEach
    @DisplayName("initialization")
    public  void  setup(){
        recursiver = new Recursiver();
    }


    @Test
    @DisplayName("Test recursive search works")
    public void testRecursiveSearch()
    {
//      Given       When        Then
        assertEquals(1,recursiver.recursiveSearch(new int[]  {1,2,3,4},2));
        assertEquals(-1,recursiver.recursiveSearch(new int[]  {1,2,3,4},5));
        assertEquals(-1,recursiver.recursiveSearch(new int[]  {1,2,3,4},0));
        assertEquals(3,recursiver.recursiveSearch(new int[]  {1,2,3,4},4));
// fail("always start with a fail");
    }



        @Test
    @DisplayName("How recursive works-Adding values of an Array")
    public void testRecursive()
    {
//      Given       When        Then
       assertEquals(1,recursiver.recursiveSumUpValues(new int[]  {1}));
       assertEquals(55,recursiver.recursiveSumUpValues(new int[]  {1, 2, 3, 4, 5, 6, 7, 8, 9,10}));
       assertEquals(6,recursiver.recursiveSumUpValues(new int[]  {1, 2, 3}));
//        fail("Always start with a fail");

        assertAll(
                ()->recursiver.recursiveSumUpValues(new int[]{1, 2}),
                ()->recursiver.recursiveSumUpValues(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9,10})

        );
    }

    @Test
    public  void testLong(){
        long val = 123;
        Long smth = Long.valueOf(val);

        Class<?> x =  smth.getClass();


        Stack<Integer> stack = new Stack<>();
        stack.push(10);
        stack.push(20);
        stack.push(30);
        stack.size();
       int last = stack.pop();

        stack.size();
        System.out.println("size of the stack is"+stack.size());

    }

    private void tst(Class<?> cl){
        String name = cl.getName();
    }






}
