package com.sodogan.patterns.dev.codechallenge;

public class Recursiver
{

    public int recursiveSumUpValues(int[] values)
    {
        return RecursionUtility.recursionOnArray(values, 0);
    }

    public int recursiveSearch(int[] values, int value){
        return  RecursionUtility.recursiveSearch(values,value,0);
    }

    //    Recursive Helper
    static class RecursionUtility
    {

        static int recursionOnArray(int[] values, int index)
        {
            if (index >= values.length) return 0;
            return values[index] + recursionOnArray(values, ++index);
        }

        //        Recursive factorial
        static int factorial(int value)
        {
            if (value == 1) return 1;
            return value * factorial(value - 1);
        }

        //        Recursive summation
        static int summation(int value)
        {
            if (value == 0) return 0;
            return value + factorial(value - 1);
        }

//        search
        static int recursiveSearch(int[] values,int value, int index){
            if(index >= values.length) return -1;
            if(values[index] == value) return  index;
            return  recursiveSearch(values,value,++index);
        }

    }

}
