package com.sodogan.patterns.dev.codechallenge;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class TestArraySearcher
{

//  Cases: for integers
//        1-Array is null return index -1
//        2- Item in the Array return the index found
//        3-Item not in the array return -1
//        4- Item occurs multiple times ?
    private ArraySearcher arraySearcher;

    @BeforeEach
    public void setup()
    {
        arraySearcher = new ArraySearcher();
    }

    @Test
    @DisplayName("Given a value and a NULL Array When search Then Exception is thrown")
    public void searchInANullArray()
    {
//     Given a value and a NULL Array
        int value = 10;
        int[] intArray = null;
//     When-search
// Then should throw an exception
//       Assertions.assertDoesNotThrow(()->arraySearcher.search(value, intArray));
       Assertions.assertThrows(NullPointerException.class,()->arraySearcher.search(value, intArray));
        //        fail("Always start with  a fail");

    }

    @Test
    @DisplayName("Given a value NOT IN Array When search Then index -1(not found)")
    public void ItemIsNotINArray()
    {
//     Given a value and a NULL Array
        int value = 10;
        int[] intArray = {9};
//     When-search
        int actual = arraySearcher.search(value, intArray);
// Then should return 0
        assertEquals(-1,actual,"Item is in the array");

//        fail("Start with a fail");
    }

    @Test
    @DisplayName("Given a value  IN Array When search Then index denotes the found)")
    public void ItemIsINArray(){
        //     Given a value and a NULL Array
        int value = 10;
        int[] intArray = {10};
//     When-search
        int actual = arraySearcher.search(value, intArray);
//     Then
        int expected = 0;
        assertEquals(expected,actual,"Item should be found");


//        fail("Start with a fail");
    }


    @Test
    @DisplayName("Given a value  at the Last ındex IN Array When search Then index denotes the found)")
    public void ItemIsINLastIndex(){
        //     Given a value and a NULL Array
        int value = 5;
        int[] intArray = {2,3,4,6,4,9,7,43,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,1,5};

//     When-search
        int actual = arraySearcher.search(value, intArray);
        System.out.println(String.format("Array length: %s | Number of iterations:  %s",intArray.length,arraySearcher.getNumberOfIterations()));
//     Then
        int expected = 30;
       assertEquals(expected,actual,"Item should be found");


//       fail("Start with a fail");
    }



}
