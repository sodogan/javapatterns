package com.sodogan.patterns.dev.codechallenge;

import com.sodogan.patterns.dev.decorator.Anchovies;

public class ArraySearcher
{

    public static final int NOT_FOUND = -1;


    private int numberOfIterations = 0;

    public  int  search(int val, int[] anyArray){
//      int index = 0;
       if(anyArray == null)
        throw new NullPointerException("The Array is Null");

       for(int i = 0; i <anyArray.length;i++){
           if(val == anyArray[i]) {
               return i;
           }
       }
//       while (true){
//           if(index >= anyArray.length)
//               break;
//           if(val == anyArray[index])
//             return index;
//
//           index ++;
//           numberOfIterations++;
//       }

       return NOT_FOUND;
    }

    public int getNumberOfIterations()
    {
        return numberOfIterations;
    }


}
