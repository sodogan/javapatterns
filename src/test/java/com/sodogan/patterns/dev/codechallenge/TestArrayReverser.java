package com.sodogan.patterns.dev.codechallenge;


import static org.junit.jupiter.api.Assertions.*;

import com.sodogan.patterns.dev.objectpool.ObjectPool;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

public class TestArrayReverser
{

    private ArrayReverser<Double> arrayReverser;


    @Test
    @BeforeEach
    public void setup()
    {
        arrayReverser = new ArrayReverser();
    }




    @Test
    @DisplayName("Given:<all same element Array>" +
            "When:<Reverse> " +
            "Then:<Return the same  array> ")
    public void reverseSameElementArray()
    {
//        Given
        Integer [] intArray = new Integer[]{3,3,3,3,3};
//        when
        Integer[] actual = arrayReverser.reverse(new Integer[]{3,3,3,3,3});
//        Then
        assertArrayEquals(new Integer[]{3,3,3,3,3},actual,"Given:<ascending same element Array>\"");

//       fail(new AssertionFailedException());
    }


    @Test
    @DisplayName("Given:<5 ascending element Array>" +
            "When:<Reverse> " +
            "Then:<Return the reversed  array> ")
    public void reverseAscendingElementArray()
    {
//        Given
        Double [] doubleArray = {1d,3.5d,5.6d,7.567};
//        when
        Double[] actual = arrayReverser.reverse(doubleArray);
//        Then
       assertArrayEquals(new Double[]{7.567,5.6d,3.5d,1d},actual,"Given:<ascending element Array>\"");

//       fail(new AssertionFailedException());
    }


    @Test
    @DisplayName("Given:<2 element Array>" +
            "When:<Reverse> " +
            "Then:<Return the reversed  array> ")
    public void reverse2elementArray()
    {
//        Given
        Integer[] intArray = {3,1};
//        when
        Integer[] actual = arrayReverser.reverse(intArray);
//        Then
        assertArrayEquals(new Integer[]{1,3},actual,"Given:<2 element Array>\"");

//        fail(new AssertionFailedException());
    }


    @Test
    @DisplayName("Given:<1 element Array>" +
                 "When:<Reverse> " +
                 "Then:<Return the same  array> ")
    public void reverse1elementArray()
    {

        Integer[] intArray = {100};
//        Given:<1 element Array> When:<Reverse>
        Integer [] actual = arrayReverser.reverse(intArray);
//        Then <Return the same  array>
        Integer[] expected = {100};
        assertArrayEquals(expected, actual,"");
//        fail(new AssertionFailedException());
    }


    @Test
    @DisplayName("Given:<Null Array>" +
                 "When:<Reverse> " +
                 "Then:<Return a null array> ")
    public void canNotReverseEmptyArray()
    {
        Integer[] intArray = null;

//        Integer s  = ((Integer[]) new Object[]{});

//     Given-<Null Array>- When:<Reverse>
        Integer [] actual = arrayReverser.reverse(intArray);
//      Then-<Return an empty array>
        Integer[] expected = null;
       assertArrayEquals(expected,actual, "Given null array-return empty array" );
//        fail(new AssertionFailedException());
    }



    //Helper class
    class AssertionFailedException extends AssertionFailedError
    {
        public static final String DEFAULT_CASE = "Always Start with a fail";

        public AssertionFailedException()
        {
            super(DEFAULT_CASE);
        }

        public AssertionFailedException(String cause)
        {
            super(cause);
        }


    }


}
