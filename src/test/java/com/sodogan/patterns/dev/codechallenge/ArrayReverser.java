package com.sodogan.patterns.dev.codechallenge;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ArrayReverser<T extends Number>
{


    public <T> T[] reverse(T[] anyArray)
    {
        if (anyArray == null) {
//            return (T[])new Object[]{};
            return  anyArray;
        }

        int backwardIndex, forwardIndex = 0;
        T[] result = anyArray;

        backwardIndex = anyArray.length - 1;

        while (backwardIndex > forwardIndex) {
            if (result[forwardIndex] != result[backwardIndex])
                swap(backwardIndex, forwardIndex, result);
            backwardIndex--;
            forwardIndex++;
        }
        return result;
    }

    private <T> void swap(int backwardIndex, int forwardIndex, T[] result)
    {
        T temp = result[forwardIndex];
        result[forwardIndex] = result[backwardIndex];
        result[backwardIndex] = temp;
    }


}
