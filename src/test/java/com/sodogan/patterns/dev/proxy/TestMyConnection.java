package com.sodogan.patterns.dev.proxy;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.naming.AuthenticationException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class TestMyConnection
{


    public static final String JDBC_MYSQL_LOCALHOST_3306_WORLD  = "jdbc:mysql://localhost:3306/world";
    public static final String JDBC_MYSQL_LOCALHOST_3306_SAKILA = "jdbc:mysql://localhost:3306/sakila";

    private IMyConnection connection;
    private Properties    properties;

    @BeforeEach
    public void setup()
    {
        properties = new Properties();
        properties.put("user", "root");
        properties.put("password", "Taksim12");
//        Connection mockConnection = mock(Connection.class);
    }

    @Test
    @DisplayName("how will you test with Mockito")
    public void howToTestWithMockito() throws SQLException, AuthenticationException
    {
       Connection mockConnection = mock(Connection.class);
//   how to inject the mock db into the Connection
//   how to inject the mock?
//        Connection mockConnection = MyDependencyInjector.injectConnection(Connection.class);
         injectConnection(Connection.class);
        IMyConnection realConnection = new MyConnection(JDBC_MYSQL_LOCALHOST_3306_WORLD, properties);

//     test it now
//        doReturn(true).when(mockConnection).isClosed();
//        doReturn(true).when(mockWebService).searchISBN("1234");
        assertTrue(realConnection.getConnection().isClosed());

    }

    private static <T> Connection injectConnection(Class<T> classToMock)
    {
        Connection mockConnection = (Connection) mock(Connection.class);
        MyDependencyLookupFactory.injectConnection( mockConnection);
         return  mockConnection;
    }


    @Test
    @DisplayName("testing MyConnection with right username password")
    public void testMyConnectionWithRightUser() throws SQLException
    {
//Given
        try {
            connection = new MyConnection(JDBC_MYSQL_LOCALHOST_3306_WORLD, properties);
        } catch (SQLException exception) {
            exception.printStackTrace();
            fail("The connection  has failed with: " + exception.getMessage());
        }
//        Then there is a connection already
        assertTrue(connection.validate());
//        When you close the Connection
        connection.close();
// Then
        assertFalse(connection.validate());

        //        fail("Always start with a fail");

    }

    @Test
    @DisplayName("testing MyConnection with empty username password")
    public void testMyConnectionWithWrongUser() throws SQLException
    {
//Given
        properties.clear();
        try {
            connection = new MyConnection(JDBC_MYSQL_LOCALHOST_3306_WORLD, properties);
        } catch (SQLException exception) {
//            exception.printStackTrace();
            fail("The connection  has failed with: " + exception.getMessage());
        }
//        Then there is a connection already
        assertTrue(connection.validate());
//        When you close the Connection
        connection.close();
// Then
        assertFalse(connection.validate());

        //        fail("Always start with a fail");

    }


    @Test
    @DisplayName("testing MyConnection with same db-it creates twice")
    public void testMyConnectionWithMultipleDB() throws SQLException
    {
//Given
        IMyConnection newConnection = null;
        try {
            connection = new MyConnection(JDBC_MYSQL_LOCALHOST_3306_WORLD, properties);
            newConnection = new MyConnection(JDBC_MYSQL_LOCALHOST_3306_WORLD, properties);
        } catch (SQLException exception) {
            exception.printStackTrace();
            fail("The connection  has failed with: " + exception.getMessage());
        }
//        Then there is a connection already
        assertTrue(connection.validate());
        assertTrue(newConnection.validate());
// Then
        assertEquals(connection, newConnection, "Should be the same connection");

        //        fail("Always start with a fail");

    }


}
