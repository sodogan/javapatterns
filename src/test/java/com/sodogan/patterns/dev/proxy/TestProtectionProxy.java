
package com.sodogan.patterns.dev.proxy;

import static org.junit.jupiter.api.Assertions.*;

import com.mysql.cj.exceptions.AssertionFailedException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.naming.AuthenticationException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Properties;

public class TestProtectionProxy
{
    public static final String JDBC_MYSQL_LOCALHOST_3306_WORLD = "jdbc:mysql://localhost:3306/world";
    Properties properties = new Properties();

    @Test
    @DisplayName("Testing a valid user connecting to the DB with protection proxy-They share the same interface")
    public void validUserTrytoConnect() throws SQLException
    {
        Connection actualConnection = null;

        properties.clear();
        properties.put("user","root");
        properties.put("password","Taksim12");


//      Given
        IMyConnection protectionProxy = new MyConnectionProtectionProxy(JDBC_MYSQL_LOCALHOST_3306_WORLD,properties);
//  When
        try {
          actualConnection = protectionProxy.connect();
        }
        catch (Exception exception){
            fail(exception.getMessage());
        }
//Then
     assertTrue(actualConnection.isValid(1000));

    }


    @Test
    @DisplayName("Testing an invalid user connecting to the DB with protection proxy-They share the same interface")
    public void inValidUserTrytoConnect() throws SQLException
    {
        Connection actualConnection = null;

        properties.clear();
        properties.put("user","wronguser");
        properties.put("password","Taksim");


//      Given
        IMyConnection protectionProxy = new MyConnectionProtectionProxy(JDBC_MYSQL_LOCALHOST_3306_WORLD,properties);
//  When
//        try {
//            actualConnection = protectionProxy.connect();
//        }
//        catch (Exception exception){
//            assertTrue(exception.getMessage());
//        }
//Then
        assertThrows(AuthenticationException.class,()->protectionProxy.connect());

    }




}
