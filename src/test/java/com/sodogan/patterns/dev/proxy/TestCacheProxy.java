package com.sodogan.patterns.dev.proxy;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class TestCacheProxy
{

    public static final String JDBC_MYSQL_LOCALHOST_3306_WORLD = "jdbc:mysql://localhost:3306/world";
    public static final String JDBC_MYSQL_LOCALHOST_3306_SAKILA = "jdbc:mysql://localhost:3306/sakila";
    private Properties    properties;

    private IMyConnection myCacheConnection;
    @BeforeEach
    public void setup()
    {
        properties = new Properties();
        properties.put("user", "root");
        properties.put("password", "Taksim12");

        myCacheConnection = new MyConnectionCacheProxy(JDBC_MYSQL_LOCALHOST_3306_WORLD,properties);

    }


    @Test
    @DisplayName("Testing the cache proxy caches the Connections")
    public void testCacheProxyCanCache(){
//when
        Connection connectionReal = null;
        Connection connectionSame = null;
        try {
        connectionReal =    myCacheConnection.connect();
        connectionSame =    myCacheConnection.connect();
        } catch (Exception exception) {
            fail("Connection failed with"+ exception.getMessage());
        }
//Then
        assertEquals(connectionReal,connectionSame,"No need to recreate");
//    fail("Always start with a fail");
    }

    @Test
    @DisplayName("Testing the cache proxy can connect to DB")
    public void testCacheProxyCanConnect(){
//when
        try {
            myCacheConnection.connect();
        } catch (Exception exception) {
            fail("Connection failed with"+ exception.getMessage());
        }
//Then
//        Assertions.fail("Failed");

    }






}
