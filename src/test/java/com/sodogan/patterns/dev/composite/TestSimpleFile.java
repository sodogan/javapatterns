package com.sodogan.patterns.dev.composite;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


class TestSimpleFile
{


    //    assertAll("many",
////                ()-> assertTrue(true),
////                ()-> assertEquals(1,1),
////                ()-> assertFalse(()-> 2 > 3),
////                ()->assertArrayEquals(new int[]{1, 2}, new int[]{1, 2})
////
    @Test
    @DisplayName("Testing the Simple File-Getters")
    public void testingSimpleFileGetters()
    {
//        GIVEN
        String filePath = "C:\\temp\\t1.txt";
//        WHEN
        SimpleFile simpleFile = new SimpleFile(filePath);
//        THEN
        String expected = filePath;
        String actual = simpleFile.getAbsolutePath();
        assertAll(
                () -> assertEquals(expected, actual),
                () -> assertEquals(true, simpleFile.isFile(),"t1.txt is a file"),
                () -> assertEquals(false, simpleFile.isDirectory(),"t1.txt is a file")
        );

    }

//    @Test
//    @DisplayName("File create")
//    public void canFileWithEmptyString()
//    {
//  fail(()->"it should fail-start with a fail");
//        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
//            file = new File("");
//        });
//        String fileName= null;
//        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
//            file = new File(fileName);
//        });
//
//        String actual = exception.getMessage();
//        String expected = "Unexpected";
//        assertTrue(actual.contains("For input"));
//    }


}