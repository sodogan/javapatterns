package com.sodogan.patterns.dev.composite;


import com.sun.mail.imap.protocol.ID;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class TestDirectory
{


    //    assertAll("many",
////                ()-> assertTrue(true),
////                ()-> assertEquals(1,1),
////                ()-> assertFalse(()-> 2 > 3),
////                ()->assertArrayEquals(new int[]{1, 2}, new int[]{1, 2})
////
    @Test
    @DisplayName("Testing the Directory Getters")
    public void testingDirectoryGetters()
    {
//        GIVEN
        String dirPath = "C:\\temp";
//        WHEN
        Directory directory = new Directory(dirPath);
//        THEN
        String expected = dirPath;
        String actual = directory.getDirPath();
        assertAll(
                () -> assertEquals(expected, actual),
                () -> assertEquals(0, directory.getList().size())
        );

    }

    @Test
    @DisplayName("Testing Adding File to the Directory")
    public void testingAddingFileToDirectory()
    {
//        GIVEN
        String filePath = "C:\\temp\\t1.txt";
        String dirPath = "C:\\temp";
//      AND
        Directory directory = new Directory(dirPath);
//      AND
        SimpleFile simpleFile = new SimpleFile(filePath);
//       WHEN
        directory.add(simpleFile);

//      THEN
        assertAll(
//                ()-> fail("Start with a fail")
                () -> assertEquals(1, directory.getList().size()),
                () -> assertTrue(directory.getList().contains(simpleFile))
        );

    }


    @Test
    @DisplayName("Testing adding Directory to Directory")
    public void testingAddingDirectoryToDirectory()
    {
//        GIVEN
        String mainPath = "C:\\temp";
        String dirPath1 = "C:\\temp\\t1";
        String dirPath2 = "C:\\temp\\t1\\t2";
        String dirPath3 = "C:\\temp\\t1\\t2\\t3";
//      AND
        Directory mainDir = new Directory(mainPath);
//      AND
        Directory dir1 = new Directory(dirPath1);
        String filePath1 = "C:\\temp\\t1\\t1.txt";

        Directory dir2 = new Directory(dirPath2);
        String filePath2 = "C:\\temp\\t1\\t2\\t2.txt";

        Directory dir3 = new Directory(dirPath3);
        String filePath3 = "C:\\temp\\t1\\t2\\t3\\t1.txt";

        //       WHEN
        mainDir.add(dir1);
        mainDir.add(dir2);
        mainDir.add(dir3);
        dir1.add(new SimpleFile(filePath1));
        dir2.add(new SimpleFile(filePath2));
        dir3.add(new SimpleFile(filePath3));
//      THEN
        assertAll(
//                ()-> fail("Start with a fail")
                () -> assertEquals(3, mainDir.getList().size()),
                () -> assertTrue(mainDir.getList().contains(dir1))
        );

//      mainDir.ls();
        check_if_dir(mainDir);

    }

    private void check_if_dir(IDisplayable item)
    {
        if (IDisplayable.isDir(item)) {
            Directory directory = (Directory) item;
        }
    }

//    @Test
//    @DisplayName("File create")
//    public void canFileWithEmptyString()
//    {
//  fail(()->"it should fail-start with a fail");
//        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
//            file = new File("");
//        });
//        String fileName= null;
//        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
//            file = new File(fileName);
//        });
//
//        String actual = exception.getMessage();
//        String expected = "Unexpected";
//        assertTrue(actual.contains("For input"));
//    }


}