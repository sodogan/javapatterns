package com.sodogan.patterns.dev.builder.meal;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class TestMealDirector
{

    @Test
    @DisplayName("Testing a Veggie Meal: VeggieBurger with Coke ")
    public void testBuildingVeggieMeal()
    {
// GIVEN
        IMealBuilder mealBuilder = new VeggieMealBuilder();
        MealDirector director = new MealDirector(mealBuilder);
// WHEN
        Meal meal = director.buildMeal();

// THEN
        verifyMeal(meal);

        double expectedCost = 8.2d;
        double actualCost = meal.getTotalCost();
        assertEquals(expectedCost, actualCost, "The total cost of a Veggie Meal");
        System.out.println(actualCost);
//        Assertions.fail("Failed");

    }






    private void verifyMeal(Meal meal)
    {
        assertNotNull(meal);
        assertTrue(meal instanceof VeggieMeal);
    }
}
