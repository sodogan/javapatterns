package com.sodogan.patterns.dev.builder;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/*
 * How to test the HouseBuilder- so many dependencies
 */
public class TestClient
{


    @Test
    @DisplayName("Create a simple house")
    public void createBasicwoodenHouse()
    {
        House house = new HouseDirector(new GlassHouseBuilder()).build();

        //       fail("Not yet");

    }
}
