package com.sodogan.patterns.dev.observer;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TestObserver
{

    //        fail("start with a fail");

    private static final String GC_BEFORE_JSON_DATA = "{ name:solen surname: dogan }";
    private static final String GC_AFTER_JSON_DATA = "{ name:solen surname: dogan }";


    @Test
    @DisplayName("what Happens When Data Changes")
    public void whatHappensWhenDataChanges()
    {

        //        Given
        IModel model = new JSONModel(GC_BEFORE_JSON_DATA);
        IView ui5View = new UI5View(model);
        IView htmlView = new HTMLView(model);

        //       When
        model.editData(GC_AFTER_JSON_DATA);

//        Then
//        Views should have been notified the change
        String lv_actual_ui5Data = ((JSONModel) ui5View.getModel()).getJsonData();
        String lv_actual_htmlData = ((JSONModel) htmlView.getModel()).getJsonData();

        assertAll("Testing views are notified",
                () -> assertEquals(GC_AFTER_JSON_DATA, lv_actual_ui5Data),
                () -> assertEquals(GC_AFTER_JSON_DATA, lv_actual_htmlData)
        );
//        fail("start with a fail");
    }

        @Test
    @DisplayName("Testing the creation of the model and the views")
    public void howToCreateTheModelandView()
    {
//        Given
        IModel model = new JSONModel(GC_BEFORE_JSON_DATA);
//       When
        IView ui5View = new UI5View(model);
        IView htmlView = new UI5View(model);
//      Then
        List<IView> expected = Arrays.asList(ui5View,htmlView);
        assertAll("Testing all parts",
                () -> assertEquals(expected, model.getListeners()),
                () -> assertEquals(false, model.isChanged())
        );
    }


}
