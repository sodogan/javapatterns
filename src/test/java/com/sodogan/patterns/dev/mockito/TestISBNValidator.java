package com.sodogan.patterns.dev.mockito;

import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.spy;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.util.LinkedList;
import java.util.List;

public class TestISBNValidator
{


    @Test
    @DisplayName("Testing ISBN Does not Exist In The List-webservice will be called")
    public void testISBNNotInTheList()
    {
//        create a mock web service
        ISBNWebService mockWebService = mock(ISBNWebService.class);
//        inject the mock
        ISBNValidator isbnValidator = new ISBNValidator(mockWebService);
        ISBNValidator spy = spy(isbnValidator);
//        This will cause to call the webservice later on
        doReturn(false).when(spy).checkAlreadyExists("1234");
        doReturn(true).when(mockWebService).searchISBN("1234");


//
//// When the real call executed
        boolean actual = spy.validateISBN("1234");
        assertTrue(actual);
        verify(mockWebService,  times(1)).searchISBN("1234");
//        fail("Always start with a fail");
    }


    @Test
    @DisplayName("Testing ISBN Already Exists In The List-If in the list webservice not called")
    public void testISBNAlreadyExistsInTheList()
    {
//        create a mock web service
        ISBNWebService mockWebService = mock(ISBNWebService.class);

//        inject the mock
        ISBNValidator isbnValidator = new ISBNValidator(mockWebService);
        ISBNValidator spy = spy(isbnValidator);
        doReturn(true).when(spy).checkAlreadyExists("1234");


// When the real call executed
        boolean actual = spy.validateISBN("1234");
        assertTrue(actual);
        verify(mockWebService, times(0)).searchISBN("1234");
    }


    @Test
    @DisplayName("Testing a valid ISBN with mockito")
    public void testValidISBNWithMockito()
    {
//        create a mock web service
        ISBNWebService mockWebService = mock(ISBNWebService.class);

//        inject the mock
        ISBNValidator isbnValidator = new ISBNValidator(mockWebService);
//      set the condition here
        doReturn(true).when(mockWebService).searchISBN("1234");
// When
        boolean actual = isbnValidator.validateISBN("1234");

        assertTrue(actual);

    }

    @Test
    @DisplayName("Testing further")
    public void testFurther()
    {
//        create a mock web service
        ISBNWebService mockWebService = mock(ISBNWebService.class);

        //        inject the mock
        ISBNValidator isbnValidator = new ISBNValidator(mockWebService);
        isbnValidator.validateISBN("1234");
        // now check if method testing was called with the parameter 12
        verify(mockWebService).searchISBN(ArgumentMatchers.eq("1234"));

        // was the method called twice?
        verify(mockWebService, times(1)).searchISBN("1234");

    }


    class InvalidISBNMock implements ISBNWebService
    {

        @Override
        public boolean searchISBN(String isbn)
        {
            return false;
        }
    }

    @Test
    @DisplayName("Testing the ISBN Validator")
    public void invalidISBN()
    {
//    Given-inject the mock
        ISBNValidator validator = new ISBNValidator(new InvalidISBNMock());
//    When
        boolean actual = validator.validateISBN("anyISBN");

        assertFalse(actual);
        //     fail("Always start with a fail");
    }


}
