package com.sodogan.patterns.dev.visitor;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;


public class TestShape
{

    @Test
    @DisplayName("Testing the creation of different Shapes")
    public void testIfShapesAreCreatedCorrectly() throws CloneNotSupportedException
    {
// Given When
        IShape rectangle = ShapeCache.getShape(IShape.Shape_Type.RECTANGLE);
        IShape square = ShapeCache.getShape(IShape.Shape_Type.SQUARE);
        IShape triangle = ShapeCache.getShape(IShape.Shape_Type.TRIANGLE);
        IShape composite = ShapeCache.getShape(IShape.Shape_Type.COMPOSITE);
        ((Composite) composite).addShape(rectangle);
        ((Composite) composite).addShape(square);
        ((Composite) composite).addShape(triangle);
//    Then
        assertAll("Testing to create the shape types",
                () -> assertTrue(rectangle instanceof Rectangle),
                () -> assertNotNull(rectangle),
                () -> assertTrue(square instanceof Square),
                () -> assertNotNull(square),
                () -> assertTrue(triangle instanceof Triangle),
                () -> assertNotNull(triangle),
                () -> assertTrue(composite instanceof Composite),
                () -> assertNotNull(composite)

        );

        composite.draw();

    }

    @Test
    @DisplayName("Testing the export functinality")
    public void testExportingShapesCorrectly() throws CloneNotSupportedException
    {
//        Given
        IShape rectangle = ShapeCache.getShape(IShape.Shape_Type.RECTANGLE);
//       When
        String xml_str = testExport(rectangle);
//      Then
       assertTrue(xml_str.contains("rectangle"));
    }


    private String testExport(IShape shape){
        XMLExporterVisitor exporter = new XMLExporterVisitor();
        return exporter.exportToXML(shape);
    }


}
