package com.sodogan.patterns.dev.decorator;

import static org.junit.jupiter.api.Assertions.*;

import com.sodogan.patterns.dev.builder.meal.VeggieMeal;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestPizzaStore
{

    @Test
    @DisplayName("Testing just pizza by itself")
    public  void  testJustPizzaByItself(){
//GIVEN
        AbstractPizza margarita = new Margarita();
        AbstractPizza meatlovers = new MeatLovers();
        AbstractPizza vegeterian  = new Vegeterian();
//WHEN
//       THEN
        assertAll("testing cost of plain pizza types",
                ()->assertEquals(PriceList.MARGARITA.getPrice(),margarita.getCost()),
                ()->assertEquals(PriceList.MEATLOVERS.getPrice(),meatlovers.getCost()),
                ()->assertEquals(PriceList.VEGETERIAN.getPrice(), vegeterian.getCost())
                );
//        fail("Not yet implemented");
    }

    @Test
    @DisplayName("Testing Margarita with Mushrooms")
    public  void  testMargaritaWithMushroms(){
//GIVEN
        AbstractPizza margarita = new Margarita();
        AbstractPizza mushyMargarita = new Mushrooms( margarita);
//WHEN
        double expected = PriceList.MARGARITA.getPrice() + PriceList.MUSHROOMS.getPrice();

        assertEquals(expected,mushyMargarita.getCost(),"Margarita with Mushy is cheap");
//        fail("Not yet implemented");
    }


    @Test
    @DisplayName("Testing Margarita with Pepperoni and Mushroom and jalapeno")
    public  void  testWithMushromDecorator(){

//GIVEN
        AbstractPizza margarita = new Margarita();
        AbstractPizza pepperoni = new Pepperoni(margarita);

        AbstractPizza mushyPepperoni = new Mushrooms( pepperoni);
        AbstractPizza mushyChilliPepperoni = new Jalepeno( mushyPepperoni);


        mushyChilliPepperoni.getCost();
//WHEN
        double expected = PriceList.MARGARITA.getPrice() +
                          PriceList.PEPPERONI.getPrice() +
                          PriceList.MUSHROOMS.getPrice() +
                          PriceList.JALAPENO.getPrice() ;

        assertEquals(expected,mushyChilliPepperoni.getCost(),"Margarita with Mushy is cheap");
//        fail("Not yet implemented");
    }





}
