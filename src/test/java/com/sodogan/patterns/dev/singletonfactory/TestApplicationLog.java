package com.sodogan.patterns.dev.singletonfactory;

import org.junit.jupiter.api.*;

import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.*;

public class TestApplicationLog
{


    @Test
    @DisplayName("Testing each class has one and only one Log Level")
    public void test_each_class_should_have_one_log_class() throws LogExistsException
    {
//    Given-When
        SimpleLogger _log1 = (SimpleLogger) LogCache.addLog(TestApplicationLog.class, SimpleLogger.ALL);
        SimpleLogger _log2 = (SimpleLogger) LogCache.addLog(ILog.class, SimpleLogger.DEBUG);

         _log1.add();
//	Then
// log2 should still be returning the same object
        assertNotEquals(_log1, _log2,"Both log objects should be same");
// The list should contain one value for TestApplicationLog.clas
        assertEquals(2, LogCache.getLogList().size());

        //		fail("Start with a fail");
    }



}
