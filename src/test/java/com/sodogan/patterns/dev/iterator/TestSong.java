package com.sodogan.patterns.dev.iterator;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

public class TestSong
{
    private Song anySong;

    @BeforeEach
    public void setup()
    {
        anySong = new Song("Beatles", "hey Jude", Genre.ALTERNATIVE);
        System.out.println(anySong.getSongInfo());
    }

    @Test
    @DisplayName("Testing the removing from the Array")
    public void testSongOf70s()
    {
//      fail("Start with a fail");
        //        Given
        IGenericCollection<Song> bestOf70sSongs = new BestOf70SArrayCollection();
        //       When empty
        bestOf70sSongs.add(anySong);
        bestOf70sSongs.add(anySong);
        Iterator<Song> iterator =  bestOf70sSongs.iterator();

        while (iterator.hasNext()){
            Song current = (Song) iterator.next();
            System.out.println(anySong.getSongInfo());
        }
//TODO:why the toarray throws exception here!
        for(Song song:bestOf70sSongs){
            System.out.println(anySong.getSongInfo());

        }
        //        Song[] actualList = (Song[]) bestOf70sSongs.toArray();
//        Song[] expectedList = {anySong};
//        assertArrayEquals(expectedList, actualList);

        // When removed wrong index
//        actual = bestOf70sSongs.remove(2);
//        assertEquals(false, actual);
//        // When removed wrong index
//        actual = bestOf70sSongs.remove(0);
//        assertEquals(true, actual);
//        testAsAClient(bestOf80sSongs);

    }




    //    how will client access-Very Messy is not it especially Array
    void testAsAClient(IGenericCollection playList)
    {
//        int index = 0;
//        if (playList instanceof BestOf70SArrayCollection) {
//            ISong[] songOfArray = ((BestOf70SArrayCollection<ISong>) playList).getPlaylist();
//
//            while (index < songOfArray.length) {
//                ISong song = songOfArray[index];
//                System.out.println("Displaying  the 70s List");
//                song.getSongInfo();
//                index++;
//            }
//
//        } else {
//            List<ISong> songOfList = ((BestOf80SListCollection) playList).getPlaylist();
//            while (index < songOfList.size()) {
//                ISong song = songOfList.get(index);
//                System.out.println("Displaying  the 80s List");
//                song.getSongInfo();
//                index++;
//            }
//        }

    }


}
