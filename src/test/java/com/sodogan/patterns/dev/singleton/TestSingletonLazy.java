/**
 * 
 */
package com.sodogan.patterns.dev.singleton;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Version;
import org.apache.logging.log4j.message.ParameterizedMessageFactory;


/**
 * @author solen
 *
 */
public class TestSingletonLazy 
{

	private SingletonLazy _lazy1;
	private SingletonLazy _lazy2;

	private static final Logger _logger = LogManager.getLogger(TestSingletonEager.class,
			new ParameterizedMessageFactory());


	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	public void setUp() throws Exception
	{
		_logger.info("SETUP-Setting up the test class: {}", Version.getProductString());
		_lazy1 = SingletonLazy.getInstance();
		_lazy2 = SingletonLazy.getInstance();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	public void tearDown() throws Exception
	{
		_logger.info("TEARDOWN-Deleting the test class");
		_lazy1 = null;
		_lazy2 = null;

	}

	/**
	 * Test method for
	 * {@link com.sodogan.patterns.dev.singleton.SingletonLazy#getInstance()}.
	 */

	@Test
	public final void testGetIsInitialized()
	{
		_logger.info("TestGetIsInitialized flag: {}", SingletonLazy.IsInitialized());
		assertTrue( SingletonLazy.IsInitialized(),"Should be true!");
	}

	@Test
	public final void testOneInstance()
	{
		assertEquals(_lazy1, _lazy2);
		// clear
		_lazy1 = null;
		SingletonLazy _lazy1 = SingletonLazy.getInstance();
		assertEquals(_lazy1, _lazy2);
		// startWithFail();
	}

}
