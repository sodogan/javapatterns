/**
 * 
 */
package com.sodogan.patterns.dev.singleton;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;


import java.io.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Version;
import org.apache.logging.log4j.message.ParameterizedMessageFactory;

/**
 * @author solen
 *
 */
public class TestSingletonSerializable
{

	private static final String FILENAME_SER = "filename.ser";

	private SingletonSerializable _serializable1;

	private ObjectOutput _out;

	private static final Logger _logger = LogManager.getLogger(TestSingletonSerializable.class,
			new ParameterizedMessageFactory());



	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	public void setUp() throws Exception
	{
		_logger.info("SETUP-Setting up the test class: {}", Version.getProductString());
		_serializable1 = SingletonSerializable.getInstance();

		_out = new ObjectOutputStream(new FileOutputStream(FILENAME_SER));
		_out.writeObject(_serializable1);
		_out.close();

	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	public void tearDown() throws Exception
	{
		_logger.info("TEARDOWN-Deleting the test class");
		_serializable1 = null;

	}

	/**
	 * Test method for
	 * {@link com.sodogan.patterns.dev.singleton.SingletonEager#getInstance()}.
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws ClassNotFoundException 
	 */
	@Test
	public final void testGetInstance() throws FileNotFoundException, IOException, ClassNotFoundException
	{
		_logger.info("TESTGETINSTANCE-Testing the instance");
		//deserialize from file to object
        ObjectInput in = new ObjectInputStream(new FileInputStream(
                FILENAME_SER));
        SingletonSerializable _serializable2 = (SingletonSerializable) in.readObject();
        
        assertEquals(_serializable1.hashCode(), _serializable2.hashCode(),"Should be the same object");
        
        //close the file!
        in.close();
		
//		  fail("Not yet implemented"); 

	}

}
