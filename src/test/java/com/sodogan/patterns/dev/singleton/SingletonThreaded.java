package com.sodogan.patterns.dev.singleton;

public class SingletonThreaded
{

    private SingletonLazy singleton;


    public  void testMultiThreaded(){

        Thread thread1 = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
             singleton = SingletonLazy.getInstance();
            }
        });
//accessing the same Singleton
        Thread thread2 = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                singleton = SingletonLazy.getInstance();
            }
        });

//        start the both threads
        thread1.start();
        thread2.start();


    }

    public SingletonLazy getSingleton()
    {
        return singleton;
    }


}
