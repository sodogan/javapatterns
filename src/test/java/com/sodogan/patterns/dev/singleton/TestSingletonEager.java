/**
 * 
 */
package com.sodogan.patterns.dev.singleton;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Version;
import org.apache.logging.log4j.message.ParameterizedMessageFactory;

/**
 * @author solen
 *
 */
public class TestSingletonEager
{

	private SingletonEager _eager1;
	private SingletonEager _eager2;

	private static final Logger _logger = LogManager.getLogger(TestSingletonEager.class,
			new ParameterizedMessageFactory());


	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	public void setUp() throws Exception
	{
		_logger.info("SETUP-Setting up the test class: {}", Version.getProductString());
		_eager1 = SingletonEager.getInstance();
		_eager2 = SingletonEager.getInstance();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	public void tearDown() throws Exception
	{
		_logger.info("TEARDOWN-Deleting the test class");

		_eager1 = null;
		_eager2 = null;

	}

	/**
	 * Test method for
	 * {@link com.sodogan.patterns.dev.singleton.SingletonEager#getInstance()}.
	 */
	@Test
	public final void testGetInstance()
	{
		_logger.info("TESTGETINSTANCE-Testing the instance");
//		fail("Not yet implemented"); // TODO
		assertEquals(_eager1, _eager2);

	}

	@Test
	public final void testObjectNotNull()
	{
		_logger.info("TESTOBJECTNOTNULL-Testing the instance");
		assertNotNull(_eager1);
		assertNotNull(_eager2);
//		fail("Not yet implemented"); // TODO
	}

}
