package com.sodogan.patterns.dev.singleton;

import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestMultiThreaded
{

    @Test
    public void testSingletonEagerWithConcurrency() throws InterruptedException
    {
        int numberOfThreads = 1000000;
        ExecutorService service = Executors.newFixedThreadPool(10);
        CountDownLatch latch = new CountDownLatch(numberOfThreads);
        final SingletonLazy[] actual = new SingletonLazy[numberOfThreads];
        for (int i = 0; i < numberOfThreads; i++) {
            int finalI = i;
            service.submit(() ->
            {
                actual[finalI] = SingletonLazy.getInstance();
                latch.countDown();
            });
        }
        latch.await();
        for(SingletonLazy lazy: actual){
            assertEquals(SingletonLazy.getInstance(), lazy);

        }

    }



}
