package com.sodogan.patterns.dev.nullobject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;


import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.*;

public class TestNullObject
{

    public static final int SALARY = 100;

    @Test
    @DisplayName("Trying to get Aussie Tax")
    public void canGetAussieTax()
    {
//    Given
        ITax aussieTax = TaxFactory.getTaxByCountry(CountryName.AUSTRALIA);
//    When
        double actual = aussieTax.calculate_tax(SALARY);
//    Then
        double expected = SALARY * ITax.AUSSIE_TAX_RATE;

        assertEquals(expected,actual,"Aussie tax is high");
//        fail("Always start with a fail");
    }

    @Test
    @DisplayName("Trying to get Danish Tax")
    public void canGetDanishTax()
    {
        //    Given
        ITax danishTax = TaxFactory.getTaxByCountry(CountryName.DENMARK);
//    When
        double actual = danishTax.calculate_tax(SALARY);
//    Then
        double expected = SALARY * ITax.DANISH_TAX_RATE;

        assertEquals(expected,actual,"Danish tax is high");


//        fail("Always start with a fail");
    }

    @Test
    @DisplayName("Trying to get Null Tax")
    public void canGetNullTax()
    {
        //    Given-Thailand is not in the list
        ITax nullTax = TaxFactory.getTaxByCountry(CountryName.THAILAND);
//    When
        double actual = nullTax.calculate_tax(SALARY);
//    Then-should return Null tax
        double expected = 0;

        assertEquals(expected,actual,"Null tax is high");




//        fail("Always start with a fail");
    }

}
