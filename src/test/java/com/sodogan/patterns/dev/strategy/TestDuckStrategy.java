package com.sodogan.patterns.dev.strategy;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

public class TestDuckStrategy
{

    /*
     * injected behaviour
     */
    public  class lcl_injected_fly_behaviour implements  IFlyBehaviour
    {
        private  static final int FLIGHT_HEIGHT = 100;

        @Override
        public int performFly()
        {
          return FLIGHT_HEIGHT;
        }


    }

    @Test
    @DisplayName("Inject a fly behavoiur and see")
    public void canInjectAFlyBehaviour(){
//    Given
        Duck injected = new DecoyDuck(new lcl_injected_fly_behaviour(),new NullSoundBehaviour() );
//    When
        injected.performFly();

    }

    @Test
    @DisplayName("Testing Fly Behaviour-Mallard that can fly high behaviour")
    public void mallardThatCanFlyHighAndQuack()
    {
        //Given
        Duck mallard = new MallardDuck(
                new CanFlyHighBehaviour(),
                new CanQuackBehaviour());
//		When
        mallard.performFly();
//      THEN
        assertEquals(IFlyBehaviour.Flight_Height.HIGH.getHeight(), mallard.getFlyBehaviour().getHeight());
//       fail("Not yet implemented");

    }

    @Test
    @DisplayName("Testing Quack Behaviour-decoy ducks does not have a quack")
    public void mallardThatCanFlyHighButNotQuack()
    {
        //Given
        Duck mallard = new DecoyDuck(
                new NullFlyBehaviour(),
                new CanSqueakBehaviour()
        );
//		When
        IQuackBehaviour.QuackType quackType= mallard.performQuack();
//      THEN
        assertEquals(IQuackBehaviour.QuackType.SQEEK, quackType);
//       fail("Not yet implemented");

    }


    @Test
    @DisplayName("Testing dynamic change of behaviours-Mallard that can fly then no fly")
    public void mallardThatCanChangeFlyBehaviourDynamically()
    {
        //Given
        Duck mallard = new MallardDuck(
                new CanFlyHighBehaviour(),
                new NullSoundBehaviour());
//		When
        mallard.performFly();
//      THEN
        assertEquals(IFlyBehaviour.Flight_Height.HIGH.getHeight(), mallard.getFlyBehaviour().getHeight());
//		When-Change the flight behaviour here!
        mallard.setFlyBehaviour(new NullFlyBehaviour());
//      THEN
        IFlyBehaviour currentBehaviour = mallard.getFlyBehaviour();
        assertEquals(IFlyBehaviour.Flight_Height.NONE.getHeight(), currentBehaviour.getHeight());


    }






}
