package com.sodogan.patterns.dev.playground;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;

public class TestStudent
{


    @Test
    public void testSmth()
    {

//        List<Student> studentList = Arrays.asList(new Student(), new Student());
        Deque<Integer> deque = new ArrayDeque<>();
       for(int i = 0;i < 10; i++){
           deque.push(i);
       }
//        Now print it
        deque.poll();

        for(Integer value:deque){
            System.out.println(value);
        }
//        deque.pop();

    }


    @Test
    @DisplayName("testing the student")
    public void howDoICreateAStudent()
    {
//        Given
        Student student1 = new Student();
        Student student2 = new Student();
//       fluent API
        StudentReceiver receiver = new StudentReceiver();
        receiver.testMultiThread(student1.setID(12).setName("eren").setAddress("istanbul").setID(1),
                student2.setID(12).setName("solen").setAddress("istanbul").setID(99)
        );
        System.out.println(receiver.getStudentInfo());
//        Thread t1 = new Thread(() -> student.setID(12).setName("solen").setAddress("istanbul"));
//        Thread t2 = new Thread(() -> student.setID(12).setName("solen").setAddress("istanbul"));


    }


}
