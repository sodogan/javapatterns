package com.sodogan.patterns.dev.playground;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class TestCofR
{
    enum Operation
    {
        ADD,
        MULTIPLY,
        SUBTRACT
    }

    ;

    class Request
    {

        private int[] numbers;
        private Operation operation;

        public Request( Operation operation,int... numbers)
        {
            this.numbers = numbers;
            this.operation = operation;
        }

        public int[] getNumbers()
        {
            return numbers;
        }

        public void setNumbers(int[] numbers)
        {
            this.numbers = numbers;
        }

        public Operation getOperation()
        {
            return operation;
        }

        public void setOperation(Operation operation)
        {
            this.operation = operation;
        }




    }


    interface IChainHandler
    {
        public void setNextChain(IChainHandler handler);
        public abstract int calculate(Request request);
    }

    abstract class AbstractChainHandler{

        protected  AbstractChainHandler nextHandler;
        public  AbstractChainHandler(AbstractChainHandler handler){
            nextHandler = handler;
        }
        public abstract int calculate(Request request);

    }

    class Multiply extends  AbstractChainHandler
    {

        public Multiply(AbstractChainHandler handler)
        {
            super(handler);
        }


        @Override
        public int calculate(Request request)
        {
            if (request.getOperation() == Operation.MULTIPLY) {
                int total = 1;
                for (int val : request.getNumbers()) {
                    total *= val;
                }
                return total;
            } else
                return nextHandler.calculate(request);
        }
    }

    class Add extends  AbstractChainHandler
    {

        public Add(AbstractChainHandler handler)
        {
            super(handler);
        }

        @Override
        public int calculate(Request request)
        {
            if (request.getOperation() == Operation.ADD) {

                int total = 0;
                for (int val : request.getNumbers()) {
                    total += val;
                }
                return total;
            } else
                return nextHandler == null ? 0 : nextHandler.calculate(request);
        }
    }


    class Subtract extends  AbstractChainHandler
    {

        public Subtract(AbstractChainHandler handler)
        {
            super(handler);
        }

        @Override
        public int calculate(Request request)
        {
            if (request.getOperation() == Operation.SUBTRACT) {

                int total = 0;
                for (int val : request.getNumbers()) {
                    total = val - total;
                }
                return total;
            } else
                return nextHandler == null ? 0 : nextHandler.calculate(request);
        }
    }

    class Invoker{

        private final AbstractChainHandler headHandler;

        public  Invoker(AbstractChainHandler handler){
            this.headHandler = handler;
        }

        public  int calculate(Request request){
            return headHandler.calculate(request);
        }
    }

    @Test
    @DisplayName("testing the chain handler")
    public void testChainHandler()
    {

        AbstractChainHandler headHandler = new Multiply( new Add(new Subtract(null)));

        Invoker invoker = new Invoker(headHandler);

        int actual = invoker.calculate(new Request(Operation.SUBTRACT, 23, 5, 6));

        assertEquals(12, actual);
//        List<IChainHandler> list = Arrays.asList(multiply, add);
//        calculateAll(list);


//        fail("what faiied?");
    }

//    private void calculateAll(List<IChainHandler> list)
//    {
//        for (IChainHandler handler : list) {
//            int value = handler.calculate(Operation.MULTIPLY, 2, 3, 4, 5);
//            System.out.println(String.format("The calc value: %s with instance %s", value, handler.getClass()));
//        }
//
//
//    }


}
