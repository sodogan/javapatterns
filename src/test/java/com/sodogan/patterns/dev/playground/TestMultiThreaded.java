package com.sodogan.patterns.dev.playground;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestMultiThreaded
{

    @Test
    public void testArraylistWithConcurrency() throws InterruptedException
    {
        int numberOfThreads = 10000;
        ExecutorService service = Executors.newFixedThreadPool(2);
        CountDownLatch latch = new CountDownLatch(numberOfThreads);
//        List<String> list = Collections.synchronizedList(new ArrayList<>());
        Map<Integer,String > list = new ConcurrentHashMap<Integer, String>();
        for (int i = 0; i < numberOfThreads; i++) {
            int finalI = i;
            service.execute(() ->
            {
                list.put(finalI,"hello world ");
                latch.countDown();
            });
        }
        latch.await();
        assertEquals(10000, list.size());

    }


    @Test
    public void testSummationWithConcurrency() throws InterruptedException {
        int numberOfThreads = 20;
        ExecutorService service = Executors.newFixedThreadPool(10);
        CountDownLatch doneSignal = new CountDownLatch(numberOfThreads);
        MyCounter counter = new MyCounter();
        for (int i = 0; i < numberOfThreads; i++) {
            service.submit(() -> {
                try {
                    counter.increment();
                } catch (InterruptedException e) {
                    // Handle exception
                }
                doneSignal.countDown();
            });
        }
        doneSignal.await();
        assertEquals(numberOfThreads, counter.getCount());
    }

    @Test
    public void testCounterWithConcurrency() throws InterruptedException
    {
        int numberOfThreads = 10000;
        ExecutorService service = Executors.newFixedThreadPool(2);
        CountDownLatch latch = new CountDownLatch(numberOfThreads);
        MyCounter counter = new MyCounter();
        for (int i = 0; i < numberOfThreads; i++) {
            service.execute(() ->
            {
                try {
                    counter.increment();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                latch.countDown();
            });
        }
        latch.await();
        assertEquals(numberOfThreads, counter.getCount());
    }


}
