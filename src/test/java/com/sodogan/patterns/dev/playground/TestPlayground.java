package com.sodogan.patterns.dev.playground;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;

import javax.swing.text.Style;
import java.io.*;
import java.lang.reflect.Constructor;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.jupiter.api.Assertions.*;

public class TestPlayground
{



    class Person
    {

        public String getDesc()
        {
            return "I am inside the Person";
        }

        ;
    }

    class Student extends Person
    {
        private int studentID;


        public Student(String name, int studentID)
        {
            this.studentID = studentID;
        }


        @Override
        public String getDesc()
        {
            return "I am inside the Student";
        }
    }


    class Money
    {
        private final double   amount;
        private final Currency currency;

        public Money(double amount, Currency currency)
        {
            this.amount = amount;
            this.currency = currency;
        }


    }


    @FunctionalInterface
    interface  lif_any_intf{
        String getDesc();
    }

    @Test
    @DisplayName("learning new")
    public  void slinky(){
        lif_any_intf[] k = new lif_any_intf[]{
                ()->"hey",
                ()->"you you" };
        for(lif_any_intf o: k){
            System.out.println(o.getDesc());
        }

        lif_any_intf test = () -> "hello world";

        String t = "hello world ";
        System.out.println(String.format("before: length is %s",t.length()));
        String f = t.trim();
        System.out.println(String.format("After: length is %s",f.length()));

//
//        lif_any_intf[] ther = new lif_any_intf[]{
//                ()->return "hello";
//        };
//


    }


    @Test
    @DisplayName("testing Generics")
    public void testGenereics()
    {
        Student student = new Student("eren", 1234);
        List<Person> personList = Arrays.asList(new Student("sol", 12), new Person());
        List<Student> studentList = Arrays.asList(new Student("ere", 232));
        testList(studentList);
        testList(personList);


//Test from here
        //Upper Bounded Integer List
        List<Integer> list1 = Arrays.asList(4, 5, 6, 7);

        //printing the sum of elements in list
        System.out.println("Total sum is:" + sum(list1));

        //Double list
        List<Double> list2 = Arrays.asList(4.1, 5.1, 6.1);

        //printing the sum of elements in list
        System.out.print("Total sum is:" + sum(list2));


        List<Student> studs = (List<Student>) testReturn();


        List<List<? extends Number>> listofList = new ArrayList<>();
        List<Integer> intList = new ArrayList<>();
        List<Double> doubleList = new ArrayList<>();
        List<Float> floatList = new ArrayList<>();


        listofList.add(intList);
        listofList.add(doubleList);
        listofList.add(floatList);


        List<List<? extends Person>> pList = new ArrayList<>();

        pList.add(studentList);
        pList.add(personList);

        for (int index = 0; index < pList.size(); index++) {
            List<Student> sList = (List<Student>) pList.get(index);
            System.out.println(sList);


        }


    }


    public void testList(List<? extends Person> list)
    {
//        list.add(new Student("1q",1221));
//        list.add(person);
        for (Person p : list) {
            System.out.println(p.getDesc());

        }

    }


    public List<? extends Person> testReturn()
    {
        List<Student> studentList = Arrays.asList(new Student("sol", 12));
        List<Person> personList = Arrays.asList(new Person());

        return studentList;
    }


    private double sum(List<? extends Number> list)
    {
        double sum = 0.0;
        for (Number i : list) {
            sum += i.doubleValue();
        }

        return sum;
    }


    @Test
    @DisplayName("testing Stack")
    public void StackIsImmutable()
    {
        Object[] srcs =
                {new int[4], new double[4], new Integer[4], new Number[4], new String[4], new Object[]{"Graal", 0, 0, 0}, new Object()};


        List<String> applications = new ArrayList<>();
        applications.add("Singleton");

        List<Money> ref = new ArrayList<>();
        ref.add(new Money(1.2, Currency.getInstance(Locale.CANADA)));

//     list of lists
        List<? extends Number> listList = new ArrayList<>();
//        listList.add(applications);
//        listList.add(ref);
//        listList.add(Double.valueOf(1.2d));
//     fail( "start with a fail");
        assertEquals(2, listList.size());

//    List<String> recovered= listList.get(0);

    }


    @Test
    @DisplayName("testing List ımmutability")
    public void ListIsImmutable()
    {
        List<Money> singletonList = Collections.singletonList(new Money(1.2, Currency.getInstance(Locale.CANADA)));
        Map<Integer, Money> singletonHashList =
                Collections.singletonMap(1, new Money(1.2, Currency.getInstance(Locale.CANADA)));

        // When try to add new entry should throw exception
        assertThrows(UnsupportedOperationException.class, () -> singletonList.add(new Money(1.2, Currency.getInstance(Locale.CANADA))));

    }

    @Test
    @DisplayName("testing ımmutability")
    public void StringIsImmutable()
    {
//        fail("start with a fail");
        String expected = "Hello world";
        expected.replace("world", "json");

        assertEquals(expected, "Hello json");
    }

    @Test
    @DisplayName("Learning new things")
    public void testSmth() throws Throwable
    {

        FileOutputStream fileOutputStream = new FileOutputStream("C:\\temp\\t1.txt");

        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
        String str = "huhuhuhu";
//        bufferedOutputStream.write(str.getBytes());
//        bufferedOutputStream.close();

        PrintStream printStream = new PrintStream(fileOutputStream);
        printStream.println(str);

        printStream.close();
//        bufferedOutputStream.close();
        fileOutputStream.close();

        //
//        PrintStream s = System.out;
//        s.println();

//        StringBuilder builder = new StringBuilder();
//        builder.append("Helloo");
//        builder.append("its me");
//
//        PrintStream test = System.err;
//        test.println("are you well?");
//
//        Throwable throwable = new RuntimeException();
//        String [] args = {"p1","p2","p3"};
//
//        AbstractMessageFactory messageFactory = SimpleMessageFactory.INSTANCE;
////        AbstractMessageFactory messageFactory = new ParameterizedMessageFactory();
//        Message message = messageFactory.newMessage(builder);
////        Message message = new ParameterizedMessage("",args,throwable);
//        PrintStream stream = System.err;
//
//        StatusLogger logger = StatusLogger.getLogger();
//        logger.info("Helloo");
//        logger.registerListener(new StatusConsoleListener(Level.ALL));


//        AbstractLogger simpleLogger = new SimpleLogger("simple",Level.ALL,true,messageFactory);

//        System.out.println("Get format "+message.getFormat());
//        System.out.println("Get formatted message "+message.getFormattedMessage());
//        System.out.println("get prameters  "+message.getParameters());
//        System.out.println("get throwable  "+message.getThrowable());
//        throw throwable;


    }


    <T> void printArray(T[] personArray)
    {
        for (T person : personArray) {
            System.out.println(person.toString());
        }
    }


    @Test
    @DisplayName("Coding challenge")
    public void checkOcurrence()
    {
        Map<Integer, Integer> actual;
        Map<Integer, Integer> expected;
//    Given empty array should return 0
        int[] anyArray = {};
//When
        actual = ArrayChallenge.howManyTimes(anyArray);
// Then
        expected = new ConcurrentHashMap<>();
        assertEquals(expected, actual);

//    Given array has 1 value
        anyArray = new int[]{2};
//When
        actual = ArrayChallenge.howManyTimes(anyArray);
// Then
        expected.put(2, 1);
        assertEquals(expected, actual);

//    Given array has 2 value
        anyArray = new int[]{2, 2};
//When
        actual = ArrayChallenge.howManyTimes(anyArray);
// Then
        expected.put(2, 2);
        assertEquals(expected, actual);


        //    Given array has more values
        anyArray = new int[]{1, 1, 3, 3, 5};
//When
        actual = ArrayChallenge.howManyTimes(anyArray);
// Then
        expected.put(2, 2);
        expected.put(3, 1);
//        expected.put(4,1);
//        assertEquals(expected, actual,"The most test here");


    }


    //    As + not supported for T types
    private <T extends Number> double add(T num1, T num2)
    {
        return num1.doubleValue() + num2.doubleValue();
    }

    //    Generic print
    public <T> void print(T msg)
    {
        System.out.println(msg);

    }


}
