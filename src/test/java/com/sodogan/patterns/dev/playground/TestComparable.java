package com.sodogan.patterns.dev.playground;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestComparable
{


    class Person implements Comparable<Person>
    {

        private String firstName;
        private String lastName;


        public Supplier<String> getFullName()
        {
            return fullName;
        }

        private Supplier<String> fullName = () ->
        {
            return firstName + lastName;
        };


        public Person(String firstName, String lastName)
        {
            this.firstName = firstName;
            this.lastName = lastName;
        }


        @Override
        public String toString()
        {
            return "Person{" +
                    "firstName='" + firstName + '\'' +
                    ", LastName='" + lastName + '\'' +
                    '}';
        }

        @Override
        public int compareTo(@NotNull TestComparable.Person other)
        {
            return this.firstName.toUpperCase().compareTo(other.firstName.toUpperCase());
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Person person = (Person) o;
            return firstName.equals(person.firstName) &&
                    lastName.equals(person.lastName);
        }

        @Override
        public int hashCode()
        {
            return Objects.hash(firstName, lastName);
        }
    }//endclass






    private void testBuffer(String anyString){
        System.out.println(anyString.toString());

    }


    @Test
    @DisplayName("test two objects are equal")
    public void testTwoObjectsAreEqual()
    {
        Person p1 = new Person("solen", "dogan");
        Person p2 = new Person("solen", "dogan");

//        CharSequence strBuilder = new StringBuffer().append("testing the charsequence").append("again and again");
//        testBuffer(strBuilder);
          testBuffer(String.format("hellooo %s",p1.getFullName().get())   );
//        assertEquals(p1,p2);
        System.out.println(p1.getFullName().get());
        Supplier<String> msg = () -> "what is supplier?";

//        testSupplier(() -> "what is supplier?");

//        testConsumer( s->System.out.println(s));

        Consumer<String> consumer = s->System.out.println(s);
        ((Consumer<String>)s->System.out.println(s)).accept(p1.firstName+p1.lastName);
    }

    private void testSupplier(Supplier<String> msg)
    {
        System.out.println(msg.get());
    }

    private void testConsumer(Consumer<String> msg)
    {
        Person person = new Person("sol","dogan");
        msg.accept(person.firstName);
    }


    @Test
    @DisplayName("how does sorting work")
    public void testSorting()
    {
//        Given
        List<Person> list = addPersonsToList();
//        when
        Person[] personArray = {
                new Person("solen", "fogan"),
                new Person("didar", "aogan"),
                new Person("naim", "zogan"),
                new Person("eren", "nogan"),
        };
        try {
            Arrays.sort(personArray, new Comparator<Person>()
            {
                @Override
                public int compare(Person person1, Person person2)
                {
                    return person1.lastName.compareTo(person2.lastName);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        printArray(personArray);

    }

    <T> void printArray(T[] personArray)
    {
        for (T person : personArray) {
            System.out.println(person.toString());
        }
    }

    @NotNull
    private List<Person> addPersonsToList()
    {
        List<Person> list = new ArrayList<>();
        String default_fname = "AnyFirstName";
        String default_lname = "AnyLastName";
        String defaultName;
        while (true) {
            defaultName = default_fname + "-" + list.size();
            if (list.size() == 100) {
                break;
            } else {
                list.add(new Person(defaultName, default_lname));
            }
            defaultName = "";
        }
        return list;
    }

    @Test
    @DisplayName("Coding challenge")
    public void checkOcurrence()
    {
        Map<Integer, Integer> actual;
        Map<Integer, Integer> expected;
//    Given empty array should return 0
        int[] anyArray = {};
//When
        actual = ArrayChallenge.howManyTimes(anyArray);
// Then
        expected = new ConcurrentHashMap<>();
        assertEquals(expected, actual);

//    Given array has 1 value
        anyArray = new int[]{2};
//When
        actual = ArrayChallenge.howManyTimes(anyArray);
// Then
        expected.put(2, 1);
        assertEquals(expected, actual);

//    Given array has 2 value
        anyArray = new int[]{2, 2};
//When
        actual = ArrayChallenge.howManyTimes(anyArray);
// Then
        expected.put(2, 2);
        assertEquals(expected, actual);


        //    Given array has more values
        anyArray = new int[]{1, 1, 3, 3, 5};
//When
        actual = ArrayChallenge.howManyTimes(anyArray);
// Then
        expected.put(2, 2);
        expected.put(3, 1);
//        expected.put(4,1);
//        assertEquals(expected, actual,"The most test here");


    }


    //    As + not supported for T types
    private <T extends Number> double add(T num1, T num2)
    {
        return num1.doubleValue() + num2.doubleValue();
    }

    //    Generic print
    public <T> void print(T msg)
    {
        System.out.println(msg);

    }


}
